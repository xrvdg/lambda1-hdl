\begin{code}
import Data.MapAccum
import Data.SimpleTypes


import Lambda1.Gates
import Lambda1.Gates.Bool
import Lambda1.Gates.Nat

import Lambda1.Lambda1
import Lambda1.State
import Lambda1.BasicCombinators
import Lambda1.Patterns
import Lambda1.Patterns.Properties

import Lambda1.Simulation
import Lambda1.Simulation.Bool
import Lambda1.Simulation.Nat


import Lambda1.Surface.Lambda1
import Lambda1.Surface.State
import Lambda1.Surface.Patterns
import Lambda1.Surface.Patterns.Properties
import Lambda1.Surface.Simulation


import Samples.BoolTrioComb
import Samples.SharingAndLooping
import Samples.Adders
import Samples.Nat
\end{code}
