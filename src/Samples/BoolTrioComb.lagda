\begin{code}
module Samples.BoolTrioComb where

open import Function using (flip)
open import Data.Star using (_◅_)
open import Data.Vec using (foldl)
open import Data.Bool.Base using (Bool; true; false; _∧_; _∨_; if_then_else_) renaming (not to ¬; _xor_ to _⊻_)
open import Relation.Binary.PropositionalEquality using (_≡_; refl; module ≡-Reasoning)
open ≡-Reasoning using (begin_; _≡⟨_⟩_; _≡⟨⟩_; _∎)

open import Data.SimpleTypes using (El; vec)
open import Lambda1.Gates.Bool using (𝔹; NOT; AND; OR; MUX2; BOOLTRIO)
open import Lambda1.Lambda1 using (Val; module Lambda1WithGates)
open import Lambda1.State using (λs)
open import Lambda1.Surface.Patterns using (foldl-par)
open Lambda1WithGates BOOLTRIO using (λb)
open import Lambda1.Surface.Lambda1 using (⟨_⟩; _＄_; reIx₀; module Lambda1SurfaceWithGates)
open Lambda1SurfaceWithGates BOOLTRIO using (λh)
open import Lambda1.Surface.Simulation using (module SimulationSurfaceWithGateSim)
open import Lambda1.Simulation.Bool using (BOOLTRIOSim)
open SimulationSurfaceWithGateSim BOOLTRIOSim using (⟦_⟧Ś[_]; ⟦_⟧Ś)
\end{code}



%<*True-False>
\AgdaTarget{True, False}
\begin{code}
True False : λh 𝔹
True   = Val true
False  = Val false
\end{code}
%</True-False>

%<*True-correct>
\AgdaTarget{True-correct}
\begin{code}
True-correct : ⟦ True ⟧Ś ≡ true
True-correct = refl
\end{code}
%</True-correct>

%<*False-correct>
\AgdaTarget{False-correct}
\begin{code}
False-correct : ⟦ False ⟧Ś ≡ false
False-correct = refl
\end{code}
%</False-correct>


%<*not>
\AgdaTarget{not}
\begin{code}
not : (x : λh 𝔹) → λh 𝔹
not x = ⟨ NOT ⟩ ＄ x
\end{code}
%</not>

%<*not-correct>
\AgdaTarget{not-correct}
\begin{code}
not-correct : ∀ {x : λh 𝔹} ⦃ _ : λs x ⦄ → ⟦ not x ⟧Ś ≡ ¬ ⟦ x ⟧Ś
not-correct = refl
\end{code}
%</not-correct>



\begin{code}
infixl 6 _and_
\end{code}

%<*and>
\AgdaTarget{\_and\_}
\begin{code}
_and_ : (x y : λh 𝔹) → λh 𝔹
x and y = ⟨ AND ⟩ ＄ x ＄ y
\end{code}
%</and>

%<*And>
\AgdaTarget{And}
\begin{code}
And : ∀ {Γ} → λb (𝔹 ◅ 𝔹 ◅ Γ) 𝔹
And {Γ} = _and_ (reIx₀ (𝔹 ◅ Γ)) (reIx₀ Γ)
\end{code}
%</And>

%<*and-correct>
\AgdaTarget{and-correct}
\begin{code}
and-correct :  ∀ {x y : λh 𝔹} ⦃ _ : λs x ⦄ ⦃ _ : λs y ⦄ → ⟦ _and_ x y ⟧Ś ≡ _∧_ ⟦ x ⟧Ś ⟦ y ⟧Ś
and-correct = refl
\end{code}
%</and-correct>



\begin{code}
infixl 5 _or_
\end{code}

%<*or>
\AgdaTarget{\_or\_}
\begin{code}
_or_ : (x y : λh 𝔹) → λh 𝔹
x or y = ⟨ OR ⟩ ＄ x ＄ y
\end{code}
%</or>

%<*Or>
\AgdaTarget{Or}
\begin{code}
Or : ∀ {Γ} → λb (𝔹 ◅ 𝔹 ◅ Γ) 𝔹
Or {Γ} = _or_ (reIx₀ (𝔹 ◅ Γ)) (reIx₀ Γ)
\end{code}
%</Or>

%<*or-correct>
\AgdaTarget{or-correct}
\begin{code}
or-correct : ∀ {x y : λh 𝔹} ⦃ _ : λs x ⦄ ⦃ _ : λs y ⦄ → ⟦ _or_ x y ⟧Ś ≡ _∨_ ⟦ x ⟧Ś ⟦ y ⟧Ś
or-correct = refl
\end{code}
%</or-correct>



%<*mux2>
\AgdaTarget{mux2}
\begin{code}
mux2 : ∀ (s : λh 𝔹) (a b : λh 𝔹) → λh 𝔹
mux2 s a b = ⟨ MUX2 ⟩ ＄ s ＄ a ＄ b
\end{code}
%</mux2>

%<*Mux2>
\AgdaTarget{Mux2}
\begin{code}
Mux2 : ∀ {Γ} → λb (𝔹 ◅ 𝔹 ◅ 𝔹 ◅ Γ) 𝔹
Mux2 {Γ} = mux2 (reIx₀ (𝔹 ◅ 𝔹 ◅ Γ)) (reIx₀ (𝔹 ◅ Γ)) (reIx₀ Γ)
\end{code}
%</Mux2>


%<*mux2-spec>
\AgdaTarget{mux2-spec}
\begin{code}
mux2-spec : ∀ {α : Set} (c : El 𝔹) (a b : α) → α
mux2-spec c = flip (if_then_else_ c)
\end{code}
%</mux2-spec>

%<*mux2-correct>
\AgdaTarget{mux2-correct}
\begin{code}
mux2-correct : ∀ {c : λh 𝔹} {a b : λh 𝔹} ⦃ _ : λs c ⦄ ⦃ _ : λs a ⦄ ⦃ _ : λs b ⦄
               → ⟦ mux2 c a b ⟧Ś ≡ mux2-spec ⟦ c ⟧Ś ⟦ a ⟧Ś ⟦ b ⟧Ś
mux2-correct = refl
\end{code}
%</mux2-correct>



%<*if-then-else>
\AgdaTarget{if†\_then\_else\_}
\begin{code}
if†_then_else_ : ∀ (s : λh 𝔹) (a b : λh 𝔹) → λh 𝔹
if†_then_else_ s = flip (mux2 s)
\end{code}
%</if-then-else>

%<*Ite>
\AgdaTarget{Ite}
\begin{code}
Ite : ∀ {Γ} → λb (𝔹 ◅ 𝔹 ◅ 𝔹 ◅ Γ) 𝔹
Ite {Γ} = if†_then_else_ (reIx₀ (𝔹 ◅ 𝔹 ◅ Γ)) (reIx₀ (𝔹 ◅ Γ)) (reIx₀ Γ)
\end{code}
%</Ite>

%<*if-then-else-correct>
\AgdaTarget{if-then-else-correct}
\begin{code}
if-then-else-correct :  ∀ {c : λh 𝔹} {a b : λh 𝔹} ⦃ _ : λs c ⦄ ⦃ _ : λs a ⦄ ⦃ _ : λs b ⦄
                        → ⟦ if†_then_else_ c a b ⟧Ś ≡ if_then_else_ ⟦ c ⟧Ś ⟦ a ⟧Ś ⟦ b ⟧Ś
if-then-else-correct = refl
\end{code}
%</if-then-else-correct>




\begin{code}
infixl 5 _xor_
\end{code}

%<*xor>
\AgdaTarget{\_xor\_}
\begin{code}
_xor_ : (x y : λh 𝔹) → λh 𝔹
x xor y = (x and (not y)) or ((not x) and y)
\end{code}
%</xor>

%<*Xor>
\AgdaTarget{Xor}
\begin{code}
Xor : ∀ {Γ} → λb (𝔹 ◅ 𝔹 ◅ Γ) 𝔹
Xor {Γ} = _xor_ (reIx₀ (𝔹 ◅ Γ)) (reIx₀ Γ)
\end{code}
%</Xor>


%<*xor-denote>
\AgdaTarget{xor-denote}
\begin{code}
xor-denote : (x y : El 𝔹) → El 𝔹
xor-denote x y = x ∧ (¬ y) ∨ (¬ x) ∧ y
\end{code}
%</xor-denote>

%<*xor-denote-equiv-xor-spec>
\AgdaTarget{xor-denote≡xor-spec]
\begin{code}
xor-denote≡xor-spec : ∀ x y → xor-denote x y ≡ _⊻_ x y
xor-denote≡xor-spec false  _      = refl
xor-denote≡xor-spec true   false  = refl
xor-denote≡xor-spec true   true   = refl
\end{code}
%</xor-denote-equiv-xor-spec>

%<*xor-correct>
\AgdaTarget{xor-correct}
\begin{code}
xor-correct :  ∀ {x y : λh 𝔹} ⦃ _ : λs x ⦄ ⦃ _ : λs y ⦄ → ⟦ _xor_ x y ⟧Ś ≡ _⊻_ ⟦ x ⟧Ś ⟦ y ⟧Ś
xor-correct {x} {y} =  begin
                         ⟦ _xor_ x y ⟧Ś             ≡⟨⟩  -- by denotation of _⊻_
                         xor-denote  ⟦ x ⟧Ś ⟦ y ⟧Ś  ≡⟨ xor-denote≡xor-spec ⟦ x ⟧Ś ⟦ y ⟧Ś ⟩
                         _⊻_         ⟦ x ⟧Ś ⟦ y ⟧Ś
                       ∎
\end{code}
%</xor-correct>



%<*andN>
\AgdaTarget{andN}
\begin{code}
andN : ∀ {n} (xs : λh (vec 𝔹 n)) → λh 𝔹
andN = foldl-par _and_ True
\end{code}
%</andN>

%<*AndN>
\AgdaTarget{AndN}
\begin{code}
AndN : ∀ {n Γ} → λb (vec 𝔹 n ◅ Γ) 𝔹
AndN {n} {Γ} = andN {n} (reIx₀ Γ)
\end{code}
%</AndN>


%<*andN-spec>
\AgdaTarget{andN-spec}
\begin{code}
andN-spec : ∀ {n} → El (vec 𝔹 n) → El 𝔹
andN-spec = foldl _ _∧_ true
\end{code}
%</andN-spec>

-- TODO: also try to define the correctness and state by starting from the deep-embedded level
%<*sAndN>
\AgdaTarget{sAndN}
\begin{code}
postulate sAndN : ∀ {n Γ} (xs : λh (vec 𝔹 n)) (ss : λs {Γ = Γ} xs) → λs {Γ = Γ} (andN {n} xs)
\end{code}
%</sAndN>

%<*andN-correct>
\AgdaTarget{andN-correct}
\begin{code}
postulate andN-correct : ∀ {n} (xs : λh (vec 𝔹 n)) (ss : λs xs) → ⟦ andN {n} xs ⟧Ś[ sAndN xs ss ] ≡ andN-spec {n} ⟦ xs ⟧Ś[ ss ]
\end{code}
%</andN-correct>






%<*exotic-not>
\AgdaTarget{exotic-not}
\begin{code}
\end{code}
%</exotic-not>
exotic-not : λh 𝔹 → λh 𝔹
exotic-not x {Γ} with x {Γ}
exotic-not x | ⟨ g ⟩      = x
exotic-not x | _  = ⟨ NOT′ ⟩ ＄ x




postulate externalize : ∀ {Γ n τ} → λ₁′ Γ (vec⊗ n τ) → Vec (λ₁ τ) n
postulate internalize : ∀ {Γ n τ} → Vec (λ₁′ Γ τ) n → λ₁′ Γ (vec⊗ n τ)

sorter : ∀ {n} {τ} (_cmp_ : λ₁ τ → λ₁ τ → λ₁ (τ ⊗ τ)) (x₀ : λ₁ τ) (xs : Vec (λ₁ τ) n) → λ₁ (τ ⊗ vec⊗ n τ)
sorter _      x₀ []          {_} = x₀ ， one
sorter _cmp_  x₀ (x₁ ∷ xs′)  {Γ} = mapAccumL _cmp_ x₀ (externalize {Γ} (sorter _cmp_ x₁ xs′))
\begin{code}
\end{code}
