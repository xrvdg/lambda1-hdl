\begin{code}
module Samples.Adders where

open import Data.Bool.Base using (Bool; false; true; _∧_; _∨_) renaming (not to ¬; _xor_ to _⊻_)
open import Data.Product using (proj₂) renaming (_,_ to _,†_)
open import Data.Star using (_◅_)
open import Relation.Binary.PropositionalEquality using (_≡_; refl; cong; module ≡-Reasoning)
open ≡-Reasoning using (begin_; _≡⟨⟩_; _≡⟨_⟩_; _∎)

open import Data.Bool.Properties using (isCommutativeSemiring-∨-∧)
open import Algebra.Structures using (IsCommutativeSemiring; IsCommutativeMonoid)
open IsCommutativeSemiring isCommutativeSemiring-∨-∧ using () renaming (+-isCommutativeMonoid to ∨-isCommutativeMonoid)
open IsCommutativeMonoid ∨-isCommutativeMonoid using () renaming (identity to ∨-identity)

open import Data.SimpleTypes using (Env; El; _⊗_)
open import Lambda1.State using (λs)
open import Lambda1.Gates.Bool using (𝔹; BOOLTRIO)
open import Lambda1.Lambda1 using (module Lambda1WithGates)
open import Lambda1.Surface.Lambda1 using (⟨_⟩; let′; _,_; case⊗_of_; reIx₀; module Lambda1SurfaceWithGates)
open Lambda1WithGates BOOLTRIO using (λb)
open Lambda1SurfaceWithGates BOOLTRIO using (λh)

open import Lambda1.Surface.Simulation using (module SimulationSurfaceWithGateSim)
open import Lambda1.Simulation.Bool using (BOOLTRIOSim)
open SimulationSurfaceWithGateSim BOOLTRIOSim using (⟦_⟧S; ⟦_⟧Ś)
open import Samples.BoolTrioComb using (_and_; _or_; _xor_)

\end{code}



%<*ha>
\AgdaTarget{ha}
\begin{code}
ha : (a b : λh 𝔹) → λh ({-s-}𝔹 ⊗ {-c-}𝔹)
ha a b = (a xor b) , (a and b)
\end{code}
%</ha>

%<*Ha>
\AgdaTarget{Ha}
\begin{code}
Ha : ∀ {Γ} → λb ({-a-}𝔹 ◅ {-b-}𝔹 ◅ Γ) ({-s-}𝔹 ⊗ {-c-}𝔹)
Ha {Γ} = ha (reIx₀ (𝔹 ◅ Γ)) (reIx₀ Γ)
\end{code}
%</Ha>


%<*ha-denote>
\AgdaTarget{ha-denote}
\begin{code}
ha-denote : (a b : El 𝔹) → El ({-s-}𝔹 ⊗ {-c-}𝔹)
ha-denote a b = ((a ∧ ¬ b) ∨ (¬ a ∧ b)) ,† (a ∧ b)
\end{code}
%</ha-denote>

%<*ha-spec>
\AgdaTarget{ha-spec}
\begin{code}
ha-spec : (a b : El 𝔹) → El ({-s-}𝔹 ⊗ {-c-}𝔹)
ha-spec a b = (a ⊻ b) ,† (a ∧ b)
\end{code}
%</ha-spec>

%<*xor-not-and-or>
\AgdaTarget{xor-not-and-or}
\begin{code}
xor-not-and-or : ∀ a b → (a ∧ ¬ b) ∨ (¬ a ∧ b) ≡ a ⊻ b
xor-not-and-or false  _ = refl
xor-not-and-or true   b = (proj₂ ∨-identity) (¬ b)
\end{code}
%</xor-not-and-or>

%<*ha-correct>
\AgdaTarget{ha-correct}
\begin{code}
ha-correct : ∀ {a b : λh 𝔹} ⦃ _ : λs a ⦄ ⦃ _ : λs b ⦄ → ⟦ ha a b ⟧Ś ≡ ha-spec ⟦ a ⟧Ś ⟦ b ⟧Ś
ha-correct {a} {b} =  begin
                        ⟦ ha a b ⟧Ś              ≡⟨⟩  -- by denotation of ha
                        ha-denote ⟦ a ⟧Ś ⟦ b ⟧Ś  ≡⟨ cong (_,† ⟦ a ⟧Ś ∧ ⟦ b ⟧Ś) (xor-not-and-or ⟦ a ⟧Ś ⟦ b ⟧Ś) ⟩
                        ha-spec ⟦ a ⟧Ś ⟦ b ⟧Ś
                      ∎
\end{code}
%</ha-correct>




%<*fa>
\AgdaTarget{fa}
\begin{code}
fa : (ci a b : λh 𝔹) → λh ({-s-}𝔹 ⊗ {-co-}𝔹)
fa ci a b =  case⊗ (ha a b) of λ sab cab →
               case⊗ (ha ci sab) of λ s cabc →
                 s , (cabc or cab)
\end{code}
%</fa>

%<*Fa>
\AgdaTarget{Fa}
\begin{code}
Fa : ∀ {Γ} → λb ({-ci-}𝔹 ◅ {-a-}𝔹 ◅ {-b-}𝔹 ◅ Γ) ({-s-}𝔹 ⊗ {-co-}𝔹)
Fa {Γ} = fa (reIx₀ (𝔹 ◅ 𝔹 ◅ Γ)) (reIx₀ (𝔹 ◅ Γ)) (reIx₀ Γ)
\end{code}
%</Fa>
