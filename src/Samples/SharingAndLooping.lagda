\begin{code}
module Samples.SharingAndLooping where

open import Data.Bool.Base using (_∨_; _∧_)
open import Data.Star using (_◅_)

open import Lambda1.Lambda1 using (module Lambda1WithGates)
open import Lambda1.Surface.Lambda1 using (let′; reIx₀; loop; loop-out-next; fork; module Lambda1SurfaceWithGates)
open import Lambda1.Gates.Bool using (𝔹; BOOLTRIO)
open Lambda1WithGates BOOLTRIO using (λb)
open Lambda1SurfaceWithGates BOOLTRIO using (λh)
open import Samples.BoolTrioComb using (_and_; _or_; if†_then_else_)
\end{code}



%<*sh1>
\AgdaTarget{sh1}
\begin{code}
sh1 : (x y : λh 𝔹) → λh 𝔹
sh1 x y =  let′  z ≔ x and y
           in′   z or z
\end{code}
%</sh1>

%<*Sh1>
\AgdaTarget{Sh1}
\begin{code}
Sh1 : ∀ {Γ} → λb (𝔹 ◅ 𝔹 ◅ Γ) 𝔹
Sh1 {Γ} = sh1 (reIx₀ (𝔹 ◅ Γ)) (reIx₀ Γ)
\end{code}
%<*Sh1>


%<*sh2>
\AgdaTarget{sh2}
\begin{code}
sh2 : (x y z : λh 𝔹) → λh 𝔹
sh2 x y z =  let′  w ≔ x and y
             in′   w and z
\end{code}
%</sh2>

%<*Sh2>
\AgdaTarget{Sh2}
\begin{code}
Sh₂ : ∀ {Γ} → λb (𝔹 ◅ 𝔹 ◅ 𝔹 ◅ Γ) 𝔹
Sh₂ {Γ} = sh2 (reIx₀ (𝔹 ◅ 𝔹 ◅ Γ)) (reIx₀ (𝔹 ◅ Γ)) (reIx₀ Γ)
\end{code}
%</Sh2>


%<*shift>
\AgdaTarget{shift}
\begin{code}
shift : (i : λh 𝔹) → λh 𝔹
shift i = loop l type l ∶ 𝔹  out l
                             next i
\end{code}
%</shift>

%<*Shift>
\AgdaTarget{Shift}
\begin{code}
Shift : ∀ {Γ} → λb (𝔹 ◅ Γ) 𝔹
Shift {Γ} = shift (reIx₀ Γ)
\end{code}
%</Shift>


%<*reg>
\AgdaTarget{reg}
\begin{code}
reg : (d s : λh 𝔹) → λh 𝔹
reg d s = loop (λ s x → fork (if† s then d else x)) s
\end{code}
%</reg>

%<*Reg>
\AgdaTarget{Reg}
\begin{code}
Reg : ∀ {Γ} → λb ({-d-}𝔹 ◅ {-s-}𝔹 ◅ Γ) 𝔹
Reg {Γ} = reg (reIx₀ (𝔹 ◅ Γ)) (reIx₀ Γ)
\end{code}
%</Reg>
