\begin{code}
module Lambda1.Patterns.Properties where

open import Function using (_∘′_) renaming (_⟨_⟩_ to _≺_≻_)
open import Data.Product using (_×_; _,_; proj₁; proj₂)
open import Data.Sum using (inj₂; inj₁)
open import Data.Vec using (Vec; []; _∷_; replicate; map)
open import Data.Star using (_◅_)
open import Data.Star.Decoration using (↦)
open import Relation.Binary.PropositionalEquality using (_≡_; refl; cong; trans; cong₂)

open import Data.MapAccum using (mapAccumL2; transformF)
open import Data.SimpleTypes using (Ctxt; El; _⊗_; vec; Env)
open import Lambda1.Lambda1 using (λB[_]; Val; Unit; Loop; MapAccL-par; inj⊞; ⟨_⟩)
open import Lambda1.State using (λs; s⟨_⟩; sVal; sUnit; sLoop; sMapAccL-par; sinj⊞)
open import Lambda1.Simulation using (⟦_⊨_⟧s[_]; ⟦_⊨_⟧s₂; ⟦_⊨_⟧n[_]; Sim)
\end{code}



-- Ignoring the second parameter of sMapAccL-seq in the pattern means we don't care about the state of f itself
%<*gets0>
\AgdaTarget{gets₀}
\begin{code}
gets₀ : ∀ {B G} {Γ : Ctxt B} {σ τ} {f : λB[ G ] (σ ◅ Γ) (σ ⊗ τ)} (sm : λs (Loop f)) → El σ
gets₀ (sLoop s₀ _) = s₀
\end{code}
%</gets0>


%<*par-seq-conv>
\AgdaTarget{\_≛\_}
\begin{code}
_≛_ :  ∀ {n B G} {Γ : Ctxt B} {σ ρ τ} {f : λB[ G ] (σ ◅ ρ ◅ Γ) (σ ⊗ τ)} {e : λB[ G ] Γ σ} {xs : λB[ G ] Γ (vec ρ n)}
       (π₁ : λs (MapAccL-par f e xs) × El (σ ⊗ vec τ n)) (π₂ : λs (Loop f) × El (vec τ n)) → Set
_≛_ (_ , s′ , xs′) (sm″ , xs″) = (s′ ≡ gets₀ sm″) × (xs′ ≡ xs″)
\end{code}
%</par-seq-conv>


%<*state-free>
\AgdaTarget{state-free}
\begin{code}
state-free : ∀ {B G} {Γ : Ctxt B} {τ} (c : λB[ G ] Γ τ) (S : Sim G) → Set
state-free {Γ = Γ} c S = ∀ {γ : Env El Γ} (sa sb : λs c) → ⟦ S ⊨ c ⟧s[ sa ] γ ≡ ⟦ S ⊨ c ⟧s[ sb ] γ
\end{code}
%</state-free>


%<*,inj>
\AgdaTarget{,inj}
\begin{code}
,inj : ∀ {ℓ₁ ℓ₂} {α : Set ℓ₁} {β : Set ℓ₂} {x₁ x₂ : α} {y₁ y₂ : β} → (x₁ , y₁) ≡ (x₂ , y₂) → (x₁ ≡ x₂) × (y₁ ≡ y₂)
,inj refl = _,_ refl refl
\end{code}
%</,inj>

%<*state-free-initstates>
\AgdaTarget{state-free-initstates}
\begin{code}
state-free-initstates :  ∀ {n B G} {Γ : Ctxt B} {σ ρ τ} (f : λB[ G ] (σ ◅ ρ ◅ Γ) (σ ⊗ τ)) {e : El σ} (xs : El (vec ρ n))
                               (S : Sim G) (sfas sfbs : Vec (λs f) n) {γ : Env El Γ} (p : state-free f S)
                               → mapAccumL2 (transformF ⟦ S ⊨ f ⟧s₂ γ) e sfas xs ≡ mapAccumL2 (transformF ⟦ S ⊨ f ⟧s₂ γ) e sfbs xs
state-free-initstates f {_} []         _    []             []             {_}  _  = refl
state-free-initstates f {e} (x ∷ xs′)  S  (sfa ∷ sfas′)  (sfb ∷ sfbs′)  {γ}  p  =
  let  ih:s , ih:ys = ,inj (state-free-initstates f {e} xs′ S sfas′ sfbs′ {γ} p)
  in   ⊥
  where postulate ⊥ : _
\end{code}
%</state-free-initstates>


%<*sMapAccL-par†>
\AgdaTarget{sMapAccL-par†}
\begin{code}
sMapAccL-par† :  ∀  {n B G} {Γ : Ctxt B} {σ ρ τ} {f : λB[ G ] (σ ◅ ρ ◅ Γ) (σ ⊗ τ)}
                    (sfs : Vec (λs f) n) {s : El σ} {xs : El (vec ρ n)}
                    → λs (MapAccL-par f (Val s) (Val xs))
sMapAccL-par† {σ = σ} sfs {s} {xs} = sMapAccL-par sfs (sVal s) (sVal xs)
\end{code}
%</sMapAccL-par†>



%<*MapAccL-par-seq>
\AgdaTarget{MapAccL-par-seq}
\begin{code}
postulate  MapAccL-par-seq :
             ∀  {n B G} {Γ : Ctxt B} {σ ρ τ}
                (f : λB[ G ] (σ ◅ ρ ◅ Γ) (σ ⊗ τ)) (S : Sim G) (sf : λs f) (s : El σ) (xs : Vec (El ρ) n) (γ : Env El Γ) (p : state-free f S)
             →       ⟦ S ⊨ MapAccL-par f (Val s) (Val xs)  ⟧s[ sMapAccL-par† (replicate sf) ]     γ
                  ≛  ⟦ S ⊨ Loop f                          ⟧n[ sLoop s sf ]                    n  (map ((_◅ γ) ∘′ ↦) xs)
\end{code}
%</MapAccL-par-seq>

MapAccL-par-seq f _    _    _   []         _ _ = refl , refl
MapAccL-par-seq f S  sf₀  s₀  (x ∷ xs′)  γ p =
  let  s₁ , sf₁ , y = (transformF ⟦ S ⊨ f ⟧s₂ γ) s₀ sf₀ x
       ih:s , ih:ys = MapAccL-par-seq f S sf₁ s₁ xs′ γ p
       lem = state-free-initstates f xs′ S (replicate sf₀) (replicate sf₁) p

       s≡   = {!!}  --                   (cong proj₁             lem)  ≺ trans ≻  {!!}
       ys≡  = {!!}  -- cong₂ _∷_ refl (  (cong (proj₂ ∘′ proj₂)  lem)  ≺ trans ≻  {!!})
  in s≡ , ys≡



%<*MapAccL-par-seq′>
\AgdaTarget{MapAccL-par-seq′}
\begin{code}
\end{code}
%</MapAccL-par-seq′>
postulate
 MapAccL-par-seq′ :  ∀ {n B} {Γ : Ctxt B} {σ ρ τ}
                       (f : λB (σ ◅ ρ ◅ Γ) (σ ⊗ τ)) (sf : λs f) (s : El σ) (xs : Vec (El ρ) n) (γ : Env El Γ) (p : state-free f)
                       →     ⟦ S ⊨ MapAccL-par′  f      ⟧s′[ sMapAccL-par′     (replicate sf) ]     (↦ s ◅ ↦ xs ◅ γ)
                          ≛  ⟦ S ⊨ MapAccL-seq   (K f)  ⟧n′[ sMapAccumL-seq s  (sK sf) ]         n  (vec-envs xs γ)
