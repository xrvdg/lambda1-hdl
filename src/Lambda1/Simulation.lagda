\begin{code}
module Lambda1.Simulation where

open import Function using (flip; id; _∘′_)
open import Data.Unit.Base using (⊤)
open import Data.Product using (_×_; _,_; proj₁; proj₂; ∃₂) renaming (map to map×)
open import Data.Sum using (inj₁; inj₂; [_,_]′)
open import Data.Vec using ([]; _∷_; Vec; head; tail)
open import Data.List.Base using (List; []; _∷_)
open import Data.Stream using (Stream)
open import Data.Star using (_◅_; ε; fold)
open import Data.Star.Decoration using (↦)
open import Relation.Binary.PropositionalEquality using (_≡_)

open import Data.MapAccum using (mapAccumL2; mapAccumL1; mapAccumStream; transformF)
open import Data.SimpleTypes using (U; 𝟙; ι; El; Ctxt; Env; lookup; _⊗_; _⇒_; uncurryU; _⊇_; Env⊇)
open import Lambda1.Gates using (Gates)
open Gates using (gty)
open import Lambda1.Lambda1 using (λB[_]; ⟨_⟩; [_]; _＄_; Let; Unit; Val; Loop; _，_; Case⊗_of_; Inl; Inr; Case⊕_of_or_; Nil; Cons; Head; Tail; K)
open import Lambda1.State using (λs; _s＄_; sLet; sUnit; sVal; sLoop; _s，_; sCase⊗; sInl; sInr; sCase⊕; sNil; sCons; sHead; sTail; sK)
\end{code}



\begin{code}
record Sem {B} (F : U B → Set) (G : Gates B) : Set where
  field
    gSem     : ∀ g → F (gty G g)
    unitSem  : F 𝟙
\end{code}

\begin{code}
Sim : ∀ {B} (G : Gates B) → Set
Sim = Sem El
\end{code}

\begin{code}
open Sem
\end{code}



%<*simulation-decls>
\begin{code}
⟦_⊨_⟧s[_]  : ∀ {B G} {Γ : Ctxt B} {τ} (S : Sim G) (c : λB[ G ] Γ τ)  (s : λs c)       (γ : Env El Γ)            → (λs c × El τ)
⟦_⊨_⟧n[_]  : ∀ {B G} {Γ : Ctxt B} {τ} (S : Sim G) (c : λB[ G ] Γ τ)  (s : λs c)    n  (γn : Vec (Env El Γ) n)   → λs c × Vec (El τ) n
⟦_⊨_⟧*[_]  : ∀ {B G} {Γ : Ctxt B} {τ} (S : Sim G) (c : λB[ G ] Γ τ)  (s : λs c)       (γ* : Stream (Env El Γ))  → Stream (λs c) × Stream (El τ)
⟦_⊨_⟧S[_]  : ∀ {B G} {Γ : Ctxt B} {τ} (S : Sim G) (c : λB[ G ] Γ τ)  (s : λs c)       (γ : Env El Γ)            → El τ
⟦_⊨_⟧N[_]  : ∀ {B G} {Γ : Ctxt B} {τ} (S : Sim G) (c : λB[ G ] Γ τ)  (s : λs c)    n  (γn : Vec (Env El Γ) n)   → Vec (El τ) n
⟦_⊨_⟧★[_]  : ∀ {B G} {Γ : Ctxt B} {τ} (S : Sim G) (c : λB[ G ] Γ τ)  (s : λs c)       (γ* : Stream (Env El Γ))  → Stream (El τ)
⟦_⊨_⟧s     : ∀ {B G} {Γ : Ctxt B} {τ} (S : Sim G) (c : λB[ G ] Γ τ)  ⦃ s : λs c ⦄     (γ : Env El Γ)            → (λs c × El τ)
⟦_⊨_⟧n     : ∀ {B G} {Γ : Ctxt B} {τ} (S : Sim G) (c : λB[ G ] Γ τ)  ⦃ s : λs c ⦄  n  (γn : Vec (Env El Γ) n)   → λs c × Vec (El τ) n
⟦_⊨_⟧*     : ∀ {B G} {Γ : Ctxt B} {τ} (S : Sim G) (c : λB[ G ] Γ τ)  ⦃ s : λs c ⦄     (γ* : Stream (Env El Γ))  → Stream (λs c) × Stream (El τ)
⟦_⊨_⟧S     : ∀ {B G} {Γ : Ctxt B} {τ} (S : Sim G) (c : λB[ G ] Γ τ)  ⦃ s : λs c ⦄     (γ : Env El Γ)            → El τ
⟦_⊨_⟧N     : ∀ {B G} {Γ : Ctxt B} {τ} (S : Sim G) (c : λB[ G ] Γ τ)  ⦃ s : λs c ⦄  n  (γn : Vec (Env El Γ) n)   → Vec (El τ) n
⟦_⊨_⟧★     : ∀ {B G} {Γ : Ctxt B} {τ} (S : Sim G) (c : λB[ G ] Γ τ)  ⦃ s : λs c ⦄     (γ* : Stream (Env El Γ))  → Stream (El τ)

⟦_⊨_⟧s₁  : ∀ {B G} {Γ : Ctxt B} {σ τ}    (S : Sim G) (c : λB[ G ] (σ ◅ Γ) τ)      (s : λs c) (γ : Env El Γ)  (x : El σ)             → (λs c × El τ)
⟦_⊨_⟧s₂  : ∀ {B G} {Γ : Ctxt B} {σ ρ τ}  (S : Sim G) (c : λB[ G ] (σ ◅ ρ ◅ Γ) τ)  (s : λs c) (γ : Env El Γ)  (x : El σ) (y : El ρ)  → (λs c × El τ)
\end{code}
%</simulation-decls>



%<*simulation-step-state-explicit-defs>
\AgdaTarget{⟦\_⊨\_⟧s}
\begin{code}
⟦ S ⊨ ⟨ g ⟩ ⟧s[ s ] γ = s , (gSem S g)

⟦ S ⊨ f ＄ x ⟧s[ sf s＄ sx ] γ =
  let  sx′ , rx = ⟦ S ⊨ x ⟧s[ sx ] γ
       sf′ , rf = ⟦ S ⊨ f ⟧s[ sf ] γ
  in  (sf′ s＄ sx′) , (rf rx)

⟦ S ⊨ [ i ] ⟧s[ s ] γ = s , lookup i γ

⟦ S ⊨ Let x b ⟧s[ sLet sx sb ] γ =
  let  sx′ , rx = ⟦ S ⊨ x ⟧s[ sx ] γ
       sb′ , rb = ⟦ S ⊨ b ⟧s[ sb ] (↦ rx ◅ γ)
  in  (sLet sx′ sb′) , rb

⟦ S ⊨ Loop f ⟧s[ sLoop si sf ] γ = let sf′ , si′ , rl = ⟦ S ⊨ f ⟧s[ sf ] (↦ si ◅ γ) in sLoop si′ sf′ , rl

⟦ S ⊨ Unit ⟧s[ sUnit ] _ = sUnit , unitSem S

⟦ S ⊨ Val v ⟧s[ sVal .v ] _ = sVal v , v

⟦ S ⊨ x ， y ⟧s[ sx s， sy ] γ =
  let  sx′ , rx = ⟦ S ⊨ x ⟧s[ sx ] γ
       sy′ , ry = ⟦ S ⊨ y ⟧s[ sy ] γ
  in (sx′ s， sy′) , (rx , ry)

⟦ S ⊨ Case⊗ x∧y of f ⟧s[ sCase⊗ sxy sf ] γ =
  let  sxy′  , rx , ry  = ⟦ S ⊨ x∧y ⟧s[ sxy ]  γ
       sf′   , rz       = ⟦ S ⊨ f ⟧s₂   sf     γ rx ry
  in  (sCase⊗ sxy′ sf′) , rz

⟦ S ⊨ Inl x ⟧s[ sInl sx ] = map× sInl inj₁ ∘′ ⟦ S ⊨ x ⟧s[ sx ]
⟦ S ⊨ Inr y ⟧s[ sInr sy ] = map× sInr inj₂ ∘′ ⟦ S ⊨ y ⟧s[ sy ]

⟦ S ⊨ Case⊕ x∨y of f or g ⟧s[ sCase⊕ sxy sf sg ] γ =
  let  sxy′ , rx∨ry = ⟦ S ⊨ x∨y ⟧s[ sxy ] γ
  in  [  map×  (flip  (sCase⊕ sxy′) sg) id  ∘′ (⟦ S ⊨ f ⟧s₁ sf γ)
      ,  map×  (      (sCase⊕ sxy′) sf) id  ∘′ (⟦ S ⊨ g ⟧s₁ sg γ)
      ]′ rx∨ry


⟦ S ⊨ Nil ⟧s[ sNil ] _ = sNil , []

⟦ S ⊨ Cons x xs ⟧s[ sCons sx sxs ] γ =
  let  sx′   , rx   = ⟦ S ⊨ x ⟧s[ sx ]    γ
       sxs′  , rxs  = ⟦ S ⊨ xs ⟧s[ sxs ]  γ
  in  (sCons sx′ sxs′) , (rx ∷ rxs)

⟦ S ⊨ Head xs ⟧s[ sHead sxs ] = map× sHead head ∘′ ⟦ S ⊨ xs ⟧s[ sxs ]
⟦ S ⊨ Tail xs ⟧s[ sTail sxs ] = map× sTail tail ∘′ ⟦ S ⊨ xs ⟧s[ sxs ]
\end{code}
%</simulation-step-state-explicit-defs>


\begin{code}
⟦ S ⊨ f ⟧s₁ sf γ x    = ⟦ S ⊨ f ⟧s[ sf ] (↦ x ◅ γ)
⟦ S ⊨ f ⟧s₂ sf γ x y  = ⟦ S ⊨ f ⟧s[ sf ] (↦ x ◅ ↦ y ◅ γ)

\end{code}


\begin{code}
evalSeq : ∀ {B G} {Γ : Ctxt B} {σ ρ τ}  (f : λB[ G ] (σ ◅ ρ ◅ Γ) (σ ⊗ τ)) (S : Sim G) → (⊤ → λs (Loop f) → Env El (ρ ◅ Γ) → (⊤ × λs (Loop f) × El τ))
(evalSeq f) S u smf xγ =  u  ,  ⟦ S ⊨ Loop f ⟧s[ smf ] xγ
\end{code}



%<*simulation-step-state-instance>
\AgdaTarget{⟦\_⊨\_⟧s}
\begin{code}
⟦ S ⊨ c ⟧s ⦃ s ⦄ = ⟦ S ⊨ c ⟧s[ s ]
\end{code}
%</simulation-step-state-instance>

%<*simulation-step-nostate-explicit>
\AgdaTarget{⟦\_⊨\_⟧S[\_]}
\begin{code}
⟦ S ⊨ c ⟧S[ s ] = proj₂ ∘′ ⟦ S ⊨ c ⟧s[ s ]
\end{code}
%</simulation-step-nostate-explicit>

%<*simulation-step-nostate-instance>
\AgdaTarget{⟦\_⊨\_⟧S}
\begin{code}
⟦ S ⊨ c ⟧S ⦃ s ⦄ = ⟦ S ⊨ c ⟧S[ s ]
\end{code}
%</simulation-step-nostate-instance>


%<*simulation-vec-state-explicit>
\AgdaTarget{⟦\_⊨\_⟧n[\_]}
\begin{code}
⟦ S ⊨ c ⟧n[ s ] n = mapAccumL1 {n} ⟦ S ⊨ c ⟧s[_] s
\end{code}
%</simulation-vec-state-explicit>

%<*simulation-vec-state-instance>
\AgdaTarget{⟦\_⊨\_⟧n}
\begin{code}
⟦ S ⊨ c ⟧n ⦃ s ⦄ = ⟦ S ⊨ c ⟧n[ s ]
\end{code}
%</simulation-vec-state-instance>


%<*simulation-vec-nostate-explicit>
\AgdaTarget{⟦\_⊨\_⟧N[\_]}
\begin{code}
⟦ S ⊨ c ⟧N[ s ] n = proj₂ ∘′ ⟦ S ⊨ c ⟧n[ s ] n
\end{code}
%</simulation-vec-nostate-explicit>

%<*simulation-vec-nostate-instance>
\AgdaTarget{⟦\_⊨\_⟧N}
\begin{code}
⟦ S ⊨ c ⟧N ⦃ s ⦄ = ⟦ S ⊨ c ⟧N[ s ]
\end{code}
%</simulation-vec-nostate-instance>


%<*simulation-stream-state-explicit>
\AgdaTarget{⟦\_⊨\_⟧*[\_]}
\begin{code}
⟦ S ⊨ c ⟧*[ s ] = mapAccumStream ⟦ S ⊨ c ⟧s[_] s
\end{code}
%</simulation-stream-state-explicit>

%<*simulation-stream-state-instance>
\AgdaTarget{⟦\_⊨\_⟧*}
\begin{code}
⟦ S ⊨ c ⟧* ⦃ s ⦄ = ⟦ S ⊨ c ⟧*[ s ]
\end{code}
%</simulation-stream-state-instance>


%<*simulation-stream-nostate-explicit>
\AgdaTarget{⟦\_⊨\_⟧★[\_]}
\begin{code}
⟦ S ⊨ c ⟧★[ s ] = proj₂ ∘′ ⟦ S ⊨ c ⟧*[ s ]
\end{code}
%</simulation-stream-nostate-explicit>

%<*simulation-stream-nostate-instance>
\AgdaTarget{⟦\_⊨\_⟧★}
\begin{code}
⟦ S ⊨ c ⟧★ ⦃ s ⦄ = ⟦ S ⊨ c ⟧★[ s ]
\end{code}
%</simulation-stream-nostate-instance>




\begin{code}
module SimulationWithGateSim {B} {G : Gates B} (S : Sim G) where
\end{code}

\begin{code}
 ⟦_⟧s[_]  : ∀ {Γ τ} (c : λB[ G ] Γ τ) (s : λs c)      (γ : Env El Γ)             → (λs c × El τ)
 ⟦_⟧n[_]  : ∀ {Γ τ} (c : λB[ G ] Γ τ) (s : λs c) n    (γn : Vec (Env El Γ) n)    → (λs c × Vec (El τ) n)
 ⟦_⟧*[_]  : ∀ {Γ τ} (c : λB[ G ] Γ τ) (s : λs c)      (γ* : Stream (Env El Γ))   → (Stream (λs c) × Stream (El τ))
 ⟦_⟧S[_]  : ∀ {Γ τ} (c : λB[ G ] Γ τ) (s : λs c)      (γ : Env El Γ)             → El τ
 ⟦_⟧N[_]  : ∀ {Γ τ} (c : λB[ G ] Γ τ) (s : λs c) n    (γn : Vec (Env El Γ) n)    → Vec (El τ) n
 ⟦_⟧★[_]  : ∀ {Γ τ} (c : λB[ G ] Γ τ) (s : λs c)      (γ* : Stream (Env El Γ))   → Stream (El τ)
 ⟦_⟧s     : ∀ {Γ τ} (c : λB[ G ] Γ τ) ⦃ s : λs c ⦄    (γ : Env El Γ)             → (λs c × El τ)
 ⟦_⟧n     : ∀ {Γ τ} (c : λB[ G ] Γ τ) ⦃ s : λs c ⦄ n  (γn : Vec (Env El Γ) n)    → (λs c × Vec (El τ) n)
 ⟦_⟧*     : ∀ {Γ τ} (c : λB[ G ] Γ τ) ⦃ s : λs c ⦄    (γ* : Stream (Env El Γ))   → (Stream (λs c) × Stream (El τ))
 ⟦_⟧S     : ∀ {Γ τ} (c : λB[ G ] Γ τ) ⦃ s : λs c ⦄    (γ : Env El Γ)             → El τ
 ⟦_⟧N     : ∀ {Γ τ} (c : λB[ G ] Γ τ) ⦃ s : λs c ⦄ n  (γn : Vec (Env El Γ) n)    → Vec (El τ) n
 ⟦_⟧★     : ∀ {Γ τ} (c : λB[ G ] Γ τ) ⦃ s : λs c ⦄    (γ* : Stream (Env El Γ))   → Stream (El τ)

 ⟦_⟧s₁   : ∀ {Γ σ τ}    (c : λB[ G ] (σ ◅ Γ) τ)      (s : λs c) (γ : Env El Γ) (x : El σ)             → (λs c × El τ)
 ⟦_⟧s₂   : ∀ {Γ σ ρ τ}  (c : λB[ G ] (σ ◅ ρ ◅ Γ) τ)  (s : λs c) (γ : Env El Γ) (x : El σ) (y : El ρ)  → (λs c × El τ)
\end{code}

\begin{code}
 ⟦_⟧s[_] = ⟦ S ⊨_⟧s[_]
 ⟦_⟧n[_] = ⟦ S ⊨_⟧n[_]
 ⟦_⟧*[_] = ⟦ S ⊨_⟧*[_]
 ⟦_⟧S[_] = ⟦ S ⊨_⟧S[_]
 ⟦_⟧N[_] = ⟦ S ⊨_⟧N[_]
 ⟦_⟧★[_] = ⟦ S ⊨_⟧★[_]

 ⟦_⟧s = ⟦ S ⊨_⟧s
 ⟦_⟧n = ⟦ S ⊨_⟧n
 ⟦_⟧* = ⟦ S ⊨_⟧*
 ⟦_⟧S = ⟦ S ⊨_⟧S
 ⟦_⟧N = ⟦ S ⊨_⟧N
 ⟦_⟧★ = ⟦ S ⊨_⟧★

 ⟦_⟧s₁ = ⟦ S ⊨_⟧s₁
 ⟦_⟧s₂ = ⟦ S ⊨_⟧s₂
\end{code}




\begin{code}
postulate K-eval-eq :  ∀ {B G} {Γ Δ : Ctxt B} {τ} (S : Sim G) (c : λB[ G ] Γ τ) (s : λs c) (p : Δ ⊇ Γ) (γ : Env El Γ) (δ : Env El Δ)
                       →  let  rc = ⟦ S ⊨ c ⟧s[ s ] γ
                               rk = ⟦ S ⊨ K c {p} ⟧s[ sK s ] (Env⊇ δ γ p)
                          in  sK (proj₁ rc) ≡ proj₁ rk × proj₂ rc ≡ proj₂ rk
\end{code}




curryU : ∀ {B} (Γ : Ctxt B) (τ : U B) → U B
curryU Γ τ = fold _ _⇒_ τ Γ

postulate gateApp : ∀ {B} {σ τ : U B} (g : Gate (σ ⇒ τ)) (x : El σ) → Gate τ

uncurryEvalGate : ∀ {B} {Γ : Ctxt B} {τ} (g : Gate (curryU Γ τ)) → (Env El Γ → El τ)
uncurryEvalGate {Γ = ε}       {_} g _          = ⟦–⟧g g
uncurryEvalGate {Γ = σ ◅ Γ′}  {τ} g (↦ x ◅ e)  = uncurryEvalGate {Γ = Γ′} (gateApp g x) e 


_implements_ : ∀ {B} {Γ : Ctxt B} {τ} (c : λB Γ τ) (g : Gate (curryU Γ τ)) ⦃ s : λs c ⦄ → Set
_implements_ {Γ = Γ} {τ} c g ⦃ s ⦄ = ∀ (γ : Env El Γ) → (uncurryEvalGate g) γ ≡ ⟦ c |⟧S s γ

\begin{code}
\end{code}


⟦_|⟧S : ∀ {B} {Γ : Ctxt B} {τ} (c : λB Γ τ) (s : λs c) (γ : Env El Γ) → El τ

record Implementation () () where
  field
    Gate# : Set
    impl  : ∀ g → ∃ (λ c → c implements g)

∀ : c → Implementation G H → ⟦ c ⟧[ G ] ? ⟦ convert c ⟧[ H ]
