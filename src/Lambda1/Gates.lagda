\begin{code}
module Lambda1.Gates where

open import Data.Vec using (Vec)
open import Data.Sum using (_⊎_; [_,_]′)

open import Data.SimpleTypes using (U; El)
\end{code}



\begin{code}
record Gates (B : Set) : Set₁ where
  field
    gIx : Set
    gty : (g : gIx) → U B
\end{code}
    

\begin{code}
infixr 1 _⊞_
\end{code}

\begin{code}
_⊞_ : ∀ {B} (G : Gates B) (H : Gates B) → Gates B
G ⊞ H = record  { gIx = gIx G ⊎ gIx H
                ; gty = [ gty G , gty H ]′
                } where open Gates
\end{code}
