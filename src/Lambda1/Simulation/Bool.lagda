\begin{code}
module Lambda1.Simulation.Bool where

open import Function using (flip)
open import Data.Bool.Base using (Bool; not; _∧_; _∨_; _xor_; if_then_else_)

open import Data.SimpleTypes using (El)
open import Lambda1.Gates using (module Gates)
open import Lambda1.Gates.Bool using (BOOLTRIO; NOT; AND; OR; XOR; MUX2)
open Gates using (gty)
open import Lambda1.Simulation using (Sim)
\end{code}



\begin{code}
mux2 : ∀ (s : Bool) (x y : Bool) → Bool
mux2 s = flip (if s then_else_)
\end{code}


\begin{code}
BOOLTRIOgSem : ∀ g → El (gty BOOLTRIO g)
BOOLTRIOgSem NOT   = not
BOOLTRIOgSem AND   = _∧_
BOOLTRIOgSem OR    = _∨_
BOOLTRIOgSem XOR   = _xor_
BOOLTRIOgSem MUX2  = mux2
\end{code}


\begin{code}
BOOLTRIOSim : Sim BOOLTRIO
BOOLTRIOSim = record { gSem = BOOLTRIOgSem; unitSem = _ }
\end{code}
