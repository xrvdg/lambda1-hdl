\begin{code}
module Lambda1.Simulation.Nat where

open import Data.Nat.Base using (_+_; _*_)

open import Data.SimpleTypes using (El)
open import Lambda1.Gates using (module Gates)
open Gates using (gty)
open import Lambda1.Gates.Nat using (NAT; ADD; MUL)
open import Lambda1.Simulation using (Sim)
\end{code}



\begin{code}
NATgSem : ∀ g → El (gty NAT g)
NATgSem ADD = _+_
NATgSem MUL = _*_
\end{code}


\begin{code}
NATSim : Sim NAT
NATSim = record { gSem = NATgSem; unitSem = _ }
\end{code}
