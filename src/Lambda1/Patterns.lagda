\begin{code}
module Lambda1.Patterns where

open import Function using (_∘′_)
open import Data.Unit.Base using (tt)
open import Data.Star using (_◅_)
open import Data.Star.Decoration using (↦)
open import Data.Vec using (Vec; []; _∷_; replicate; map)
open import Data.Product using (_×_; _,_) renaming (proj₁ to p₁; proj₂ to p₂)
open import Relation.Binary.PropositionalEquality using (_≡_; refl; cong; cong₂; trans)

open import Data.MapAccum using (transformF; mapAccumL2)
open import Data.SimpleTypes using (𝟙; _⊗_; El; Ctxt; vec; Env; ◅⊇◅; ◅⊇; refl⊇)
open import Lambda1.Lambda1 using (λB[_]; ⟨_⟩; Let; Unit; Loop; _，_; Case⊗_of_; MapAccL-par; _﹩_; #₀; #₁; K; K₁)
open import Lambda1.State using (λs; s⟨_⟩; sUnit; sCase⊗; sLet; sLoop; sHead; sTail; sMapAccL-par; _s，_; _s﹩_; sK; sK′; s#₀; s#₁; sK₁; sinj⊞)
open import Lambda1.BasicCombinators using (Flip; sFlip; Units; sUnits; Fst; sFst; Snd; sSnd)
\end{code}



%<*MapAccL-par′>
\AgdaTarget{MapAccL-par′}
\begin{code}
MapAccL-par′ : ∀ {n B G} {Γ : Ctxt B} {σ ρ τ} (f : λB[ G ] (σ ◅ ρ ◅ Γ) (σ ⊗ τ)) → λB[ G ] (σ ◅ vec ρ n ◅ Γ) (σ ⊗ vec τ n)
MapAccL-par′ f = MapAccL-par (K f {wk}) #₀ #₁
  where wk = ◅⊇◅ (◅⊇◅ (◅⊇ (◅⊇ refl⊇)))
\end{code}
%</MapAccL-par′>

%<*sMapAccL-par′>
\AgdaTarget{sMapAccL-par′}
\begin{code}
sMapAccL-par′ : ∀ {n B G} {Γ : Ctxt B} {σ ρ τ} {f : λB[ G ] (σ ◅ ρ ◅ Γ) (σ ⊗ τ)} (sfs : Vec (λs f) n) → λs (MapAccL-par′ {n} f)
sMapAccL-par′ sfs = sMapAccL-par (map (sK′ {p = wk}) sfs) s#₀ s#₁
  where wk = ◅⊇◅ (◅⊇◅ (◅⊇ (◅⊇ refl⊇)))
\end{code}
%</sMapAccL-par′>

%<*sMapAccL-par-rep′>
\AgdaTarget{sMapAccL-par-rep′}
\begin{code}
sMapAccL-par-rep′ : ∀ {n B G} {Γ : Ctxt B} {σ ρ τ} {f : λB[ G ] (σ ◅ ρ ◅ Γ) (σ ⊗ τ)} (sf : λs f) → λs (MapAccL-par′ {n} f)
sMapAccL-par-rep′ = sMapAccL-par′ ∘′ replicate
\end{code}
%</sMapAccL-par-rep′>


%<*Mapper>
\AgdaTarget{Mapper}
\begin{code}
Mapper : ∀ {B G} {Γ : Ctxt B} {σ ρ τ} (f : λB[ G ] (ρ ◅ Γ) τ) → λB[ G ] (σ ◅ ρ ◅ Γ) (σ ⊗ τ)
Mapper f = #₀ ， (K₁ f)
\end{code}
%</Mapper>

%<*sMapper>
\AgdaTarget{sMapper}
\begin{code}
sMapper : ∀ {B G} {Γ : Ctxt B} {σ ρ τ} {f : λB[ G ] (ρ ◅ Γ) τ} (sf : λs f) → λs  (Mapper {σ = σ} f)
sMapper sf = s#₀ s， (sK₁ sf)
\end{code}
%</sMapper>


%<*Iterator>
\AgdaTarget{Iterator}
\begin{code}
Iterator : ∀ {B G} {Γ : Ctxt B} {σ ρ} (f : λB[ G ] (σ ◅ Γ) σ) → λB[ G ] (σ ◅ ρ ◅ Γ) (σ ⊗ σ)
Iterator f = #₀ ， Flip (K₁ f)
\end{code}
%</Iterator>

%<*sIterator>
\AgdaTarget{sIterator}
\begin{code}
sIterator : ∀ {B G} {Γ : Ctxt B} {σ ρ} {f : λB[ G ] (σ ◅ Γ) σ} (sf : λs f) → λs (Iterator {ρ = ρ} f)
sIterator sf = s#₀ s， sFlip (sK₁ sf)
\end{code}
%</sIterator>


%<*Folder>
\AgdaTarget{folder}
\begin{code}
Folder : ∀ {B G} {Γ : Ctxt B} {σ ρ} (f : λB[ G ] (σ ◅ ρ ◅ Γ) σ) → λB[ G ] (σ ◅ ρ ◅ Γ) (σ ⊗ σ)
Folder f = #₀ ， f
\end{code}
%</folder-deep>

%<*sFolder>
\AgdaTarget{sFolder}
\begin{code}
sFolder : ∀ {B G} {Γ : Ctxt B} {σ ρ} {f : λB[ G ] (σ ◅ ρ ◅ Γ) σ} (sf : λs f) → λs (Folder {ρ = ρ} f)
sFolder sf = s#₀ s， sf
\end{code}
%</sFolder>




%<*Map-par>
\AgdaTarget{Map-par}
\begin{code}
Map-par : ∀ {n B G} {Γ : Ctxt B} {ρ τ} (f : λB[ G ] (ρ ◅ Γ) τ) → λB[ G ] (vec ρ n ◅ Γ) (vec τ n)
Map-par f = Snd (MapAccL-par′ (Mapper f)  ﹩  Unit)
\end{code}
%</Map-par>

%<*sMap-par>
\AgdaTarget{sMap-par}
\begin{code}
sMap-par : ∀ {n B G} {Γ : Ctxt B} {ρ τ} {f : λB[ G ] (ρ ◅ Γ) τ} (sfs : Vec (λs f) n) → λs (Map-par {n} f)
sMap-par sfs = sSnd (sMapAccL-par′ (map sMapper sfs)  s﹩  sUnit)
\end{code}
%</sMap-par>

%<*sMap-par-rep>
\AgdaTarget{sMap-par-rep}
\begin{code}
sMap-par-rep : ∀ {n B G} {Γ : Ctxt B} {ρ τ} {f : λB[ G ] (ρ ◅ Γ) τ} (sf : λs f) → λs (Map-par {n} f)
sMap-par-rep = sMap-par ∘′ replicate
\end{code}
%</sMap-par-rep>


%<*Map-seq>
\AgdaTarget{Map-seq}
\begin{code}
Map-seq : ∀ {B G} {Γ : Ctxt B} {ρ τ} (f : λB[ G ] (ρ ◅ Γ) τ) → λB[ G ] (ρ ◅ Γ) τ
Map-seq f = Loop {σ = 𝟙} (Mapper f)
\end{code}
%</Map-seq>
-- TODO: show that map-seq f ≋ f

%<*sMap-seq>
\AgdaTarget{sMap-seq}
\begin{code}
sMap-seq : ∀ {B G} {Γ : Ctxt B} {ρ τ} {f : λB[ G ] (ρ ◅ Γ) τ} (sf : λs f) → λs (Map-seq f)
sMap-seq sf = sLoop tt (sMapper sf)
\end{code}
%</sMap-seq>



%<*Iterate-par>
\AgdaTarget{Iterate-par}
\begin{code}
Iterate-par : ∀ {n B G} {Γ : Ctxt B} {σ} (f : λB[ G ] (σ ◅ Γ) σ) → λB[ G ] (σ ◅ Γ) (σ ⊗ vec σ n)
Iterate-par f = Flip (MapAccL-par′ (Iterator f))  ﹩  Units
\end{code}
%</Iterate-par>

%<*sIterate-par>
\AgdaTarget{sIterate-par}
\begin{code}
sIterate-par : ∀ {n B G} {Γ : Ctxt B} {σ} {f : λB[ G ] (σ ◅ Γ) σ} (sfs : Vec (λs f) n) → λs (Iterate-par {n} f)
sIterate-par sfs = sFlip (sMapAccL-par′ (map sIterator sfs))  s﹩  sUnits
\end{code}
%</sIterate-par>

%<*sIterate-par-rep>
\AgdaTarget{sIterate-par-rep}
\begin{code}
sIterate-par-rep : ∀ {n B G} {Γ : Ctxt B} {σ} {f : λB[ G ] (σ ◅ Γ) σ} (sf : λs f) → λs (Iterate-par {n} f)
sIterate-par-rep = sIterate-par ∘′ replicate
\end{code}
%</sIterate-par-rep>


%<*Iterate-seq>
\AgdaTarget{Iterate-seq}
\begin{code}
Iterate-seq : ∀ {B G} {Γ : Ctxt B} {σ} (f : λB[ G ] (σ ◅ Γ) σ) → λB[ G ] (σ ◅ Γ) σ
Iterate-seq f = Loop (Iterator f)
\end{code}
%</Iterate-seq>

%<*sIterate-seq>
\AgdaTarget{sIterate-seq}
\begin{code}
sIterate-seq : ∀ {B G} {Γ : Ctxt B} {σ} {f : λB[ G ] (σ ◅ Γ) σ} (s : El σ) (sf : λs f) → λs (Iterate-seq f)
sIterate-seq s sf = sLoop s (sIterator sf)
\end{code}
%</sIterate-seq>



%<*Repeat-par>
\AgdaTarget{Repeat-par}
\begin{code}
Repeat-par : ∀ {n B G} {Γ : Ctxt B} {σ} (f : λB[ G ] (σ ◅ Γ) σ) → λB[ G ] (σ ◅ Γ) σ
Repeat-par {n} f = Fst (Iterate-par {n} f)
\end{code}
%</Repeat-par>

%<*sRepeat-par>
\AgdaTarget{sRepeat-par}
\begin{code}
sRepeat-par : ∀ {n B G} {Γ : Ctxt B} {σ} {f : λB[ G ] (σ ◅ Γ) σ} (sfs : Vec (λs f) n) → λs (Repeat-par {n} f)
sRepeat-par sfs = sFst (sIterate-par sfs)
\end{code}
%</sRepeat-par>

%<*sRepeat-par-rep>
\AgdaTarget{sRepeat-par-rep}
\begin{code}
sRepeat-par-rep : ∀ {n B G} {Γ : Ctxt B} {σ} {f : λB[ G ] (σ ◅ Γ) σ} (sf : λs f) → λs (Repeat-par {n} f)
sRepeat-par-rep = sRepeat-par ∘′ replicate
\end{code}
%</sRepeat-par-rep>


%<*Repeat-seq>
\AgdaTarget{Repeat-seq}
\begin{code}
Repeat-seq : ∀ {B G} {Γ : Ctxt B} {σ} (f : λB[ G ] (σ ◅ Γ) σ) → λB[ G ] (σ ◅ Γ) σ
Repeat-seq = Iterate-seq
\end{code}
%</Repeat-seq>

%<*sRepeat-seq>
\AgdaTarget{sRepeat-seq}
\begin{code}
sRepeat-seq : ∀ {B G} {Γ : Ctxt B} {σ} {f : λB[ G ] (σ ◅ Γ) σ} (s : El σ) (sf : λs f) → λs (Repeat-seq f)
sRepeat-seq = sIterate-seq
\end{code}
%</sRepeat-seq>



%<*Foldl-scanl-par>
\AgdaTarget{Foldl-scanl-par}
\begin{code}
Foldl-scanl-par : ∀ {n B G} {Γ : Ctxt B} {σ ρ} (f : λB[ G ] (σ ◅ ρ ◅ Γ) σ) → λB[ G ] (σ ◅ vec ρ n ◅ Γ) (σ ⊗ vec σ n)
Foldl-scanl-par f = MapAccL-par′ (Folder f)
\end{code}
%</Foldl-scanl-par>

%<*sFoldl-scanl-par>
\AgdaTarget{sFoldl-scanl-par}
\begin{code}
sFoldl-scanl-par : ∀ {n B G} {Γ : Ctxt B} {σ ρ} {f : λB[ G ] (σ ◅ ρ ◅ Γ) σ} (sfs : Vec (λs f) n) → λs (Foldl-scanl-par {n} f)
sFoldl-scanl-par sfs = sMapAccL-par′ (map sFolder sfs)
\end{code}
%</sFoldl-scanl-par>

%<*sFoldl-scanl-par-rep>
\AgdaTarget{sFoldl-scanl-par-rep}
\begin{code}
sFoldl-scanl-par-rep : ∀ {n B G} {Γ : Ctxt B} {σ ρ} {f : λB[ G ] (σ ◅ ρ ◅ Γ) σ} (sf : λs f) → λs (Foldl-scanl-par {n} f)
sFoldl-scanl-par-rep = sFoldl-scanl-par ∘′ replicate
\end{code}
%</sFoldl-scanl-par-rep>


%<*Foldl-scanl-seq>
\AgdaTarget{Foldl-scanl-seq}
\begin{code}
Foldl-scanl-seq : ∀ {B G} {Γ : Ctxt B} {σ ρ} (f : λB[ G ] (σ ◅ ρ ◅ Γ) σ) → λB[ G ] (ρ ◅ Γ) σ
Foldl-scanl-seq f = Loop (Folder f)
\end{code}
%</Foldl-scanl-seq>

%<*sFoldl-scanl-seq>
\AgdaTarget{sFoldl-scanl-seq}
\begin{code}
sFoldl-scanl-seq : ∀ {B G} {Γ : Ctxt B} {σ ρ} {f : λB[ G ] (σ ◅ ρ ◅ Γ) σ} (s : El σ) (sf : λs f) → λs (Foldl-scanl-seq f)
sFoldl-scanl-seq s sf = sLoop s (sFolder sf)
\end{code}
%</sFoldl-scanl-seq>


%<*Foldl-par>
\AgdaTarget{Foldl-par}
\begin{code}
Foldl-par : ∀ {n B G} {Γ : Ctxt B} {σ ρ} (f : λB[ G ] (σ ◅ ρ ◅ Γ) σ) → λB[ G ] (σ ◅ vec ρ n ◅ Γ) σ
Foldl-par f = Fst (Foldl-scanl-par f)
\end{code}
%</Foldl-par>

%<*sFoldl-par>
\AgdaTarget{sFoldl-par}
\begin{code}
sFoldl-par : ∀ {n B G} {Γ : Ctxt B} {σ ρ} {f : λB[ G ] (σ ◅ ρ ◅ Γ) σ} (sfs : Vec (λs f) n) → λs (Foldl-par {n} f)
sFoldl-par sfs = sFst (sFoldl-scanl-par sfs)
\end{code}
%</sFoldl-par>

%<*sFoldl-par-rep>
\AgdaTarget{sFoldl-par-rep}
\begin{code}
sFoldl-par-rep : ∀ {n B G} {Γ : Ctxt B} {σ ρ} {f : λB[ G ] (σ ◅ ρ ◅ Γ) σ} (sf : λs f) → λs (Foldl-par {n} f)
sFoldl-par-rep = sFoldl-par ∘′ replicate
\end{code}
%</sFoldl-par-rep>


%<*Scanl-par>
\AgdaTarget{Scanl-par}
\begin{code}
Scanl-par : ∀ {n B G} {Γ : Ctxt B} {σ ρ} (f : λB[ G ] (σ ◅ ρ ◅ Γ) σ) → λB[ G ] (σ ◅ vec ρ n ◅ Γ) (vec σ n)
Scanl-par f = Snd (Foldl-scanl-par f)
\end{code}
%</Scanl-par>

%<*sScanl-par>
\AgdaTarget{sScanl-par}
\begin{code}
sScanl-par : ∀ {n B G} {Γ : Ctxt B} {σ ρ} {f : λB[ G ] (σ ◅ ρ ◅ Γ) σ} (sfs : Vec (λs f) n) → λs (Scanl-par {n} f)
sScanl-par sfs = sSnd (sFoldl-scanl-par sfs)
\end{code}
%</sFoldl-par>

%<*sScanl-par-rep>
\AgdaTarget{sScanl-par-rep}
\begin{code}
sScanl-par-rep : ∀ {n B G} {Γ : Ctxt B} {σ ρ} {f : λB[ G ] (σ ◅ ρ ◅ Γ) σ} (sf : λs f) → λs (Scanl-par {n} f)
sScanl-par-rep = sScanl-par ∘′ replicate
\end{code}
%</sFoldl-par-rep>



%<*Foldl-seq-Scanl-seq>
\AgdaTarget{Foldl-seq, Scanl-seq}
\begin{code}
Foldl-seq Scanl-seq : ∀ {B G} {Γ : Ctxt B} {σ ρ} (f : λB[ G ] (σ ◅ ρ ◅ Γ) σ) → λB[ G ] (ρ ◅ Γ) σ
Foldl-seq = Foldl-scanl-seq
Scanl-seq = Foldl-scanl-seq
\end{code}
%</Foldl-seq-Scanl-seq>

%<*sFoldl-seq-sScanl-seq>
\AgdaTarget{sFoldl-seq, sScanl-seq}
\begin{code}
sFoldl-seq : ∀ {B G} {Γ : Ctxt B} {σ ρ} {f : λB[ G ] (σ ◅ ρ ◅ Γ) σ} (s : El σ) (sf : λs f) → λs (Foldl-seq f)
sScanl-seq : ∀ {B G} {Γ : Ctxt B} {σ ρ} {f : λB[ G ] (σ ◅ ρ ◅ Γ) σ} (s : El σ) (sf : λs f) → λs (Scanl-seq f)
sFoldl-seq = sFoldl-scanl-seq
sScanl-seq = sFoldl-scanl-seq
\end{code}
%</sFoldl-seq-sScanl-seq>
