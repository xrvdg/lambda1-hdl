\begin{code}
module Lambda1.Lambda1 where

open import Data.Unit.Base using (tt)
open import Function using (_∘′_; flip; const)
open import Data.Nat.Base using (ℕ; zero; suc)
open import Data.Sum using (inj₂)
open import Data.Star using (_◅_)

open import Data.Vec using (Vec; []; _∷_)

open import Data.SimpleTypes using (U; El; ι; 𝟙; _⇒_; _⊗_; _⊕_; vec; Ctxt; _∋_; ix₀; ix₁; ix₂; ix₃; ix₄; _⊇_; refl⊇; flip⊇; ◅⊇; ◅⊇◅; Kix⊇)
open import Lambda1.Gates using (Gates; _⊞_)
open Gates using (gIx; gty)
\end{code}



\begin{code}
infixr 20  _，_
infixl 2   _＄_
\end{code}

* λB is a "first-order" simply-typed lambda calculus with De Bruijn indices for vars.
 + Main differences from STLC:
   - MAINLY: No arrow types, no abs, no app.
       + REASON: All elements of λB should have a reasonably direct synthesis scheme. Higher-order terms would not.
   - Lets for sharing
   - A form of "application" can be emulated with lets. Circuits can really only be "applied" to variables
       + Anything else let-bound.
   - Dedicated constructor for feedback loops (recursion)
%<*LambdaB>
\AgdaTarget{λB[\_]}
\begin{code}
data λB[_] {B} (G : Gates B) : (Γ : Ctxt B) (τ : U B) → Set where
  ⟨_⟩    : ∀ {Γ} (g : gIx G) →  λB[ G ] Γ (gty G g)

  _＄_   : ∀ {Γ σ τ}    (f : λB[ G ] Γ (σ ⇒ τ))  (x : λB[ G ] Γ σ)
                                     → λB[ G ] Γ τ


  [_]    : ∀ {Γ τ}                 (i : Γ ∋ τ)
                                  → λB[ G ] Γ τ

  Let    : ∀ {Γ σ τ}     (x : λB[ G ] Γ σ)  (b : λB[ G ] (σ ◅ Γ) τ)
                                      → λB[ G ] Γ τ

  Loop   : ∀ {Γ σ τ}     (c : λB[ G ] (σ ◅ Γ) (σ ⊗ τ))
                                 → λB[ G ] Γ τ
\end{code}
%</LambdaB>

%<*Lambda1-unit-val>
\begin{code}
  Unit  : ∀ {Γ}                   → λB[ G ] Γ 𝟙
  Val   : ∀ {Γ τ} (v : El {B} τ)  → λB[ G ] Γ τ
\end{code}
%</Lambda1-unit-val>

%<*LambdaB-prod-coprod>
\begin{code}
  _，_           : ∀ {Γ τ υ}    (x : λB[ G ] Γ τ)  (y : λB[ G ] Γ υ)
                                        → λB[ G ] Γ (τ ⊗ υ)

  Case⊗_of_     : ∀ {Γ σ ρ τ}  (xy : λB[ G ] Γ (σ ⊗ ρ))  (f : λB[ G ] (σ ◅ ρ ◅ Γ) τ)
                                                 → λB[ G ] Γ τ

  Inl           : ∀ {Γ τ υ}    (x : λB[ G ] Γ τ) → λB[ G ] Γ (τ ⊕ υ)
  Inr           : ∀ {Γ τ υ}    (y : λB[ G ] Γ υ) → λB[ G ] Γ (τ ⊕ υ)

  Case⊕_of_or_  : ∀ {Γ σ ρ τ}  (xy : λB[ G ] Γ (σ ⊕ ρ))  (f : λB[ G ] (σ ◅ Γ) τ)  (g : λB[ G ] (ρ ◅ Γ) τ)
                                                             → λB[ G ] Γ τ
\end{code}
%</LambdaB-prod-coprod>

%<*LambdaB-vec>
\begin{code}
  Nil          : ∀ {Γ τ}              → λB[ G ] Γ (vec τ zero)

  Cons         : ∀ {n Γ τ}      (x : λB[ G ] Γ τ)  (xs : λB[ G ] Γ (vec τ n))
                                          → λB[ G ] Γ (vec τ (suc n))

  Head         : ∀ {n Γ τ}      (xs : λB[ G ] Γ (vec τ (suc n)))
                                         → λB[ G ] Γ τ

  Tail         : ∀ {n Γ τ}      (xs : λB[ G ] Γ (vec τ (suc n)))
                                     → λB[ G ] Γ (vec τ n)
\end{code}
%</LambdaB-vec>




%<*Loop-out-next>
\AgdaTarget{Loop-out-next}
\begin{code}
Loop-out-next : ∀ {B G} {Γ : Ctxt B} {σ τ} (o : λB[ G ] (σ ◅ Γ) τ) (n : λB[ G ] (σ ◅ Γ) σ) → λB[ G ] Γ τ
Loop-out-next o n = Loop (n ， o)
\end{code}
%</Loop-out-next>


\begin{code}
infixl 90 _﹩_
\end{code}$

%<*app-flipped-let>
\AgdaTarget{\_﹩\_}
\begin{code}
_﹩_ : ∀ {B G} {Γ : Ctxt B} {σ τ} (f : λB[ G ] (σ ◅ Γ) τ) (x : λB[ G ] Γ σ) → λB[ G ] Γ τ
_﹩_ = flip Let
\end{code}
%<*app-flipped-let>


%<*var-circs>
\AgdaTarget{\#₀,\#₁,\#₂,\#₃,\#₄}
\begin{code}
#₀ : ∀ {B G} {Γ : Ctxt B} {α}          → λB[ G ] (α ◅ Γ) α
#₁ : ∀ {B G} {Γ : Ctxt B} {α β}        → λB[ G ] (α ◅ β ◅ Γ) β
#₂ : ∀ {B G} {Γ : Ctxt B} {α β δ}      → λB[ G ] (α ◅ β ◅ δ ◅ Γ) δ
#₃ : ∀ {B G} {Γ : Ctxt B} {α β δ θ}    → λB[ G ] (α ◅ β ◅ δ ◅ θ ◅ Γ) θ
#₄ : ∀ {B G} {Γ : Ctxt B} {α β δ θ κ}  → λB[ G ] (α ◅ β ◅ δ ◅ θ ◅ κ ◅ Γ) κ
#₀ = [ ix₀ ];  #₁ = [ ix₁ ];  #₂ = [ ix₂ ];  #₃ = [ ix₃ ];  #₄ = [ ix₄ ]
\end{code}
%</var-circs>


%<*K>
\AgdaTarget{K}
\begin{code}
K : ∀ {B G} {Γ Δ : Ctxt B} {τ} (c : λB[ G ] Γ τ) {p : Δ ⊇ Γ} → λB[ G ] Δ τ
K ⟨ g ⟩                 {_} = ⟨ g ⟩
K (f ＄ x)              {p} = (K f {p}) ＄ (K x {p})
K [ i ]                 {p} = [ Kix⊇ p i ]
K (Let x f)             {p} = Let (K x {p}) (K f {◅⊇◅ p})
K (Loop f)              {p} = Loop (K f {◅⊇◅ p})
K Unit                  {_} = Unit
K (Val v)               {_} = Val v
K (x ， y)              {p} = K x {p} ， K y {p}
K (Case⊗ xy of f)       {p} = Case⊗ (K xy {p}) of (K f {◅⊇◅ (◅⊇◅ p)})
K (Inl x)               {p} = Inl (K x {p})
K (Inr y)               {p} = Inr (K y {p})
K (Case⊕ xy of f or g)  {p} = Case⊕ (K xy {p}) of (K f {◅⊇◅ p}) or (K g {◅⊇◅ p})
K Nil                   {_} = Nil
K (Cons x xs)           {p} = Cons (K x {p}) (K xs {p})
K (Head xs)             {p} = Head (K xs {p})
K (Tail xs)             {p} = Tail (K xs {p})
\end{code}
%</K>
K (MapAccL-par f e xs)  {p} = MapAccL-par (K f {◅⊇◅ (◅⊇◅ p)}) (K e {p}) (K xs {p})


%<*K-nums>
\AgdaTarget{K₁,K₂,K₃,K₄}
\begin{code}
K₁ : ∀ {B G} {Γ : Ctxt B} {α β}        (c : λB[ G ] Γ α) → λB[ G ] (β ◅ Γ) α
K₂ : ∀ {B G} {Γ : Ctxt B} {α β δ}      (c : λB[ G ] Γ α) → λB[ G ] (β ◅ δ ◅ Γ) α
K₃ : ∀ {B G} {Γ : Ctxt B} {α β δ θ}    (c : λB[ G ] Γ α) → λB[ G ] (β ◅ δ ◅ θ ◅ Γ) α
K₄ : ∀ {B G} {Γ : Ctxt B} {α β δ θ κ}  (c : λB[ G ] Γ α) → λB[ G ] (β ◅ δ ◅ θ ◅ κ ◅ Γ) α

K₁ c  = K c {◅⊇ refl⊇}
K₂ = K₁ ∘′ K₁;  K₃ = K₁ ∘′ K₂;  K₄ = K₁ ∘′ K₃
\end{code}
%</K-nums>






%<*Vec-elim>
\AgdaTarget{Vec-elim}
\begin{code}
Vec-elim  :  ∀ {B G} {Γ : Ctxt B} {ρ} (P : ℕ → U B) {n}
             (f : ∀ {n} →  λB[ G ] (ρ ◅ P n ◅ Γ) (P (suc n))) (e : λB[ G ] Γ (P zero)) (xs : λB[ G ] Γ (vec ρ n)) → λB[ G ] Γ (P n)
Vec-elim P {zero}   _ e _   = e
Vec-elim P {suc n}  f e xs  = f ﹩ Head (K₁ xs) ﹩ Vec-elim P f e (Tail xs) 
\end{code}
%</Vec-elim>


%<*Foldr-par>
\AgdaTarget{Foldr-par}
\begin{code}
Foldr-par : ∀ {B G} {Γ : Ctxt B} {σ ρ} {n} (f : λB[ G ] (ρ ◅ σ ◅ Γ) σ) (e : λB[ G ] Γ σ) (xs : λB[ G ] Γ (vec ρ n)) → λB[ G ] Γ σ
Foldr-par {σ = σ} f e xs = Vec-elim (const σ) f e xs
\end{code}
%</Foldr-par>

%<*Foldl-par>
\AgdaTarget{Foldl-par}
\begin{code}
Foldl-par : ∀ {n B G} {Γ : Ctxt B} {σ ρ} (f : λB[ G ] (σ ◅ ρ ◅ Γ) σ) (e : λB[ G ] Γ σ) (xs : λB[ G ] Γ (vec ρ n)) → λB[ G ] Γ σ
Foldl-par {zero}   _ e _   = e
Foldl-par {suc _}  f e xs  = Foldl-par f (f ﹩ K₁ e ﹩ Head xs) (Tail xs)
\end{code}
%</Foldl-par>


%<*MapAccL-par>
\AgdaTarget{MapAccL-par}
\begin{code}
MapAccL-par : ∀ {n B G} {Γ : Ctxt B} {σ ρ τ} (f : λB[ G ] (σ ◅ ρ ◅ Γ) (σ ⊗ τ)) (e : λB[ G ] Γ σ) (xs : λB[ G ] Γ (vec ρ n)) → λB[ G ] Γ (σ ⊗ vec τ n)
MapAccL-par {zero}    _ e _   = e ， Nil
MapAccL-par {suc _}  f e xs  =  let wk = ◅⊇ (flip⊇ (◅⊇ refl⊇))
                                in  Case⊗ f ﹩ K₁ e ﹩ Head xs of {-e′ y′-}
                                      Case⊗ MapAccL-par (K f {wk}) #₀{-e′-} (Tail (K₂ xs)) of {-e″ ys′-}
                                        #₀{-e″-} ， Cons #₃{-y′-} #₁{-ys′-}
\end{code}
%</MapAccL-par>



%<*inj⊞>
\AgdaTarget{inj⊞}
\begin{code}
inj⊞ : ∀ {B G H} {Γ : Ctxt B} {τ} (c : λB[ G ] Γ τ) → λB[ H ⊞ G ] Γ τ
inj⊞ ⟨ g ⟩                 = ⟨ inj₂ g ⟩
inj⊞ (f ＄ x)              = inj⊞ f ＄ inj⊞ x
inj⊞ [ i ]                 = [ i ]
inj⊞ (Let x e)             = Let (inj⊞ x) (inj⊞ e)
inj⊞ (Loop f)              = Loop (inj⊞ f)
inj⊞ Unit                  = Unit
inj⊞ (Val v)               = Val v
inj⊞ (x ， y)              = inj⊞ x ， inj⊞ y
inj⊞ (Case⊗ xy of f)       = Case⊗ inj⊞ xy of inj⊞ f
inj⊞ (Inl x)               = Inl (inj⊞ x)
inj⊞ (Inr y)               = Inr (inj⊞ y)
inj⊞ (Case⊕ xy of f or g)  = Case⊕ inj⊞ xy of inj⊞ f or inj⊞ g
inj⊞ Nil                   = Nil
inj⊞ (Cons x xs)           = Cons (inj⊞ x) (inj⊞ xs)
inj⊞ (Head xs)             = Head (inj⊞ xs)
inj⊞ (Tail xs)             = Tail (inj⊞ xs)
\end{code}
%</inj⊞>




\begin{code}
module Lambda1WithGates {B} (G : Gates B) where
\end{code}


\begin{code}
 λb : ∀ (Γ : Ctxt B) (τ : U B) → Set
 λb Γ τ = λB[ G ] Γ τ
\end{code}
