\begin{code}
module Lambda1.State where

open import Function using (flip; _∘′_)
open import Data.Nat.Base using (suc)
open import Data.Vec using (Vec; []; _∷_; map; replicate)
open import Data.Star using (_◅_)
open import Data.Sum using (inj₂)

open import Data.SimpleTypes using (U; El; Ctxt; _∋_; ι; _⇒_; _⊗_; _⊕_; vec; ix₀; ix₁; ix₂; ix₃; ix₄; _⊇_; Kix⊇; ◅⊇◅; ◅⊇; flip⊇; refl⊇; reIx₀∋; defaults)
open import Lambda1.Gates using (Gates; _⊞_)
open Gates using (gIx; gty)
open import Lambda1.Lambda1 using  ( ⟨_⟩; _＄_; [_]; Let; Loop; Val; Unit; _，_; Case⊗_of_; Inl; Inr; Case⊕_of_or_; Nil; Cons
                                   ; Head; Tail; MapAccL-par; λB[_]; _﹩_; #₀; #₁; #₂; #₃; #₄; K; K₁; K₂; K₃; K₄; inj⊞)
open import Lambda1.Surface.Lambda1 using (reIx₀)
\end{code}



\begin{code}
infixl 2 _s＄_
infixr 4 _s，_
\end{code}

%<*Lambda1-state>
\AgdaTarget{λs}
\begin{code}
data λs {B} {G : Gates B} : {Γ : Ctxt B} {τ : U B} (c : λB[ G ] Γ τ) → Set where
 instance
  s⟨_⟩   : ∀ {Γ}                                                  (g : gIx G)              → λs {Γ = Γ} {gty G g} ⟨ g ⟩
  _s＄_  : ∀ {Γ σ τ}  {f : λB[ G ] Γ (σ ⇒ τ)} {x : λB[ G ] Γ σ}  (sf : λs f) (sx : λs x)   → λs (f ＄ x)

  s[_]   : ∀ {Γ σ}                                                (i : Γ ∋ σ)              → λs {G = G} [ i ]
  sLet   : ∀ {Γ σ τ}  {x : λB[ G ] Γ σ} {e : λB[ G ] (σ ◅ Γ) τ}   (sx : λs x) (se : λs e)  → λs (Let x e)
  sLoop  : ∀ {Γ σ τ}  {f : λB[ G ] (σ ◅ Γ) (σ ⊗ τ)}               (si : El σ) (sf : λs f)  → λs (Loop f)

  sUnit  : ∀ {Γ}                   → λs {G = G} {Γ} Unit
  sVal   : ∀ {Γ τ} (v : El {B} τ)  → λs {G = G} {Γ} {τ} (Val v)

  _s，_   : ∀ {Γ τ υ}    {x : λB[ G ] Γ τ} {y : λB[ G ] Γ υ}                   (sx : λs x) (sy : λs y)    → λs (x ， y)
  sCase⊗  : ∀ {Γ σ ρ τ}  {xy : λB[ G ] Γ (σ ⊗ ρ)} {f : λB[ G ] (σ ◅ ρ ◅ Γ) τ}  (sxy : λs xy) (sf : λs f)  → λs (Case⊗ xy of f)

  sInl    : ∀ {Γ τ υ}     {x : λB[ G ] Γ τ}                     (sx : λs x)  → λs (Inl {υ = υ} x)
  sInr    : ∀ {Γ τ υ}     {y : λB[ G ] Γ υ}                     (sy : λs y)  → λs (Inr {τ = τ} y)
  sCase⊕  : ∀ {Γ σ ρ τ}   {xy : λB[ G ] Γ (σ ⊕ ρ)} {f : λB[ G ] (σ ◅ Γ) τ} {g : λB[ G ] (ρ ◅ Γ) τ}
                          (sxy : λs xy) (sf : λs f) (sg : λs g)  → λs (Case⊕ xy of f or g)

  sNil     : ∀ {Γ τ}                                                                             → λs {G = G} {Γ} (Nil {τ = τ})
  sCons    : ∀ {n Γ τ}   {x : λB[ G ] Γ τ} {xs : λB[ G ] Γ (vec τ n)} (sx : λs x) (sxs : λs xs)  → λs (Cons x xs)

  sHead    : ∀ {n Γ τ}   {xs : λB[ G ] Γ (vec τ (suc n))} (sxs : λs xs)   → λs (Head xs)
  sTail    : ∀ {n Γ τ}   {xs : λB[ G ] Γ (vec τ (suc n))} (sxs : λs xs)   → λs (Tail xs)
\end{code}
%</Lambda1-state>


\begin{code}
infixl 90 _s﹩_
\end{code}

%<*sApp-flipped-let>
\AgdaTarget{\_s﹩\_}
\begin{code}
_s﹩_ : ∀ {B G} {Γ : Ctxt B} {σ τ} {f : λB[ G ] (σ ◅ Γ) τ} {x : λB[ G ] Γ σ} (sf : λs f) (sx : λs x) → λs (f ﹩ x)
_s﹩_ = flip sLet
\end{code}
%</sApp-flipped-let>


%<*var-states>
\AgdaTarget{\s#₀,\s#₁,\s#₂,\s#₃,\s#₄}
\begin{code}
s#₀ : ∀ {B G} {Γ : Ctxt B} {α}          → λs {G = G} {α ◅ Γ} #₀
s#₁ : ∀ {B G} {Γ : Ctxt B} {α β}        → λs {G = G} {α ◅ β ◅ Γ} #₁
s#₂ : ∀ {B G} {Γ : Ctxt B} {α β δ}      → λs {G = G} {α ◅ β ◅ δ ◅ Γ} #₂
s#₃ : ∀ {B G} {Γ : Ctxt B} {α β δ θ}    → λs {G = G} {α ◅ β ◅ δ ◅ θ ◅ Γ} #₃
s#₄ : ∀ {B G} {Γ : Ctxt B} {α β δ θ κ}  → λs {G = G} {α ◅ β ◅ δ ◅ θ ◅ κ ◅ Γ} #₄
s#₀ = s[ ix₀ ];  s#₁ = s[ ix₁ ];  s#₂ = s[ ix₂ ];  s#₃ = s[ ix₃ ]; s#₄ = s[ ix₄ ]
\end{code}
%</var-states>


%<*sK′>
\AgdaTarget{sK′}
\begin{code}
sK′ : ∀ {B G} {Γ Δ : Ctxt B} {τ} {c : λB[ G ] Γ τ} {p : Δ ⊇ Γ} (s : λs c) → λs (K c {p})
sK′ {p = _} s⟨ g ⟩              = s⟨ g ⟩
sK′ {p = p} (sf s＄ sx)         = (sK′ {p = p} sf) s＄ (sK′ {p = p} sx)
sK′ {p = p} s[ i ]              = s[ Kix⊇ p i ]
sK′ {p = p} (sLet sx sf)        = sLet (sK′ {p = p} sx) (sK′ {p = ◅⊇◅ p} sf)
sK′ {p = p} (sLoop si sf)       = sLoop si (sK′ {p = ◅⊇◅ p} sf)
sK′ {p = _} sUnit               = sUnit
sK′ {p = _} (sVal v)            = sVal v
sK′ {p = p} (sx s， sy)         = (sK′ {p = p} sx) s， (sK′ {p = p} sy)
sK′ {p = p} (sCase⊗ sxy sf)     = sCase⊗ (sK′ {p = p} sxy) (sK′ {p = ◅⊇◅ (◅⊇◅ p)} sf)
sK′ {p = p} (sInl sx)           = sInl (sK′ {p = p} sx)
sK′ {p = p} (sInr sy)           = sInr (sK′ {p = p} sy)
sK′ {p = p} (sCase⊕ sxy sf sg)  = sCase⊕ (sK′ {p = p} sxy) (sK′ {p = ◅⊇◅ p} sf) (sK′ {p = ◅⊇◅ p} sg)
sK′ {p = _} sNil                = sNil
sK′ {p = p} (sCons sx sxs)      = sCons (sK′ {p = p} sx) (sK′ {p = p} sxs)
sK′ {p = p} (sHead sxs)         = sHead (sK′ {p = p} sxs)
sK′ {p = p} (sTail sxs)         = sTail (sK′ {p = p} sxs)
\end{code}
%</sK′>

%<*sK>
\AgdaTarget{sK}
\begin{code}
sK : ∀ {B G} {Γ Δ : Ctxt B} {τ} {c : λB[ G ] Γ τ} (s : λs c) {p : Δ ⊇ Γ} → λs (K c {p})
sK s {p} = sK′ {p = p} s
\end{code}
%</sK>


%<*sK-nums>
\AgdaTarget{sK₁,sK₂,sK₃,sK₄}
\begin{code}
sK₁ : ∀ {B G} {Γ : Ctxt B} {α β}        {c : λB[ G ] Γ α} (s : λs c) → λs {Γ = β ◅ Γ} (K₁ c)
sK₂ : ∀ {B G} {Γ : Ctxt B} {α β δ}      {c : λB[ G ] Γ α} (s : λs c) → λs {Γ = β ◅ δ ◅ Γ} (K₂ c)
sK₃ : ∀ {B G} {Γ : Ctxt B} {α β δ θ}    {c : λB[ G ] Γ α} (s : λs c) → λs {Γ = β ◅ δ ◅ θ ◅ Γ} (K₃ c)
sK₄ : ∀ {B G} {Γ : Ctxt B} {α β δ θ κ}  {c : λB[ G ] Γ α} (s : λs c) → λs {Γ = β ◅ δ ◅ θ ◅ κ ◅ Γ} (K₄ c)

sK₁ sc  = sK sc {◅⊇ refl⊇}
sK₂ = sK₁ ∘′ sK₁;  sK₃ = sK₁ ∘′ sK₂;  sK₄ = sK₁ ∘′ sK₃
\end{code}
%</sK-nums>


%<*sMapAccL-par>
\AgdaTarget{sMapAccL-par}
\begin{code}
sMapAccL-par :  ∀ {n B G} {Γ : Ctxt B} {σ ρ τ} {f : λB[ G ] (σ ◅ ρ ◅ Γ) (σ ⊗ τ)} {e : λB[ G ] Γ σ} {xs : λB[ G ] Γ (vec ρ n)}
                (sfs : Vec (λs f) n) (se : λs e) (sxs : λs xs) → λs (MapAccL-par f e xs)
sMapAccL-par []           se _    = se s， sNil
sMapAccL-par (sf ∷ sfs′)  se sxs  =
  sCase⊗ (sf s﹩ sK₁ se s﹩ sHead sxs)
    (sCase⊗ (sMapAccL-par (map sK′ sfs′) s#₀ (sK₂ (sTail sxs)))
       (s#₀ s， sCons s#₃ s#₁))
\end{code}
%</sMapAccL-par>


%<*ds>
\AgdaTarget{ds}
\begin{code}
ds : ∀ {B G} {Γ : Ctxt B} {τ} {c : λB[ G ] Γ τ} (b : B) → λs c
ds {c = ⟨ g ⟩}                _ = s⟨ g ⟩
ds {c = f ＄ x}               b = (ds {c = f} b) s＄ (ds {c = x} b)
ds {c = [ i ]}                _ = s[ i ]
ds {c = Let x e}              b = sLet (ds {c = x} b) (ds {c = e} b)
ds {c = Loop {σ = σ} f}       b = sLoop (defaults b σ) (ds {c = f} b)
ds {c = Unit}                 _ = sUnit
ds {c = Val v}                _ = sVal v
ds {c = x ， y}               b = _s，_ (ds {c = x} b) (ds {c = y} b)
ds {c = Case⊗ x∧y of f}       b = sCase⊗ (ds {c = x∧y} b) (ds {c = f} b)
ds {c = Inl x}                b = sInl (ds {c = x} b)
ds {c = Inr y}                b = sInr (ds {c = y} b)
ds {c = Case⊕ x∨y of f or g}  b = sCase⊕ (ds {c = x∨y} b) (ds {c = f} b) (ds {c = g} b)
ds {c = Nil}                  _ = sNil
ds {c = Cons x xs}            b = sCons (ds {c = x} b) (ds {c = xs} b)
ds {c = Head xs}              b = sHead (ds {c = xs} b)
ds {c = Tail xs}              b = sTail (ds {c = xs} b)
\end{code}
%</ds>


\begin{code}
sinj⊞ : ∀ {B G H} {Γ : Ctxt B} {τ} {c : λB[ G ] Γ τ} (s : λs c) → λs (inj⊞ {G = G} {H} c)
sinj⊞ s⟨ g ⟩              = s⟨ inj₂ g ⟩
sinj⊞ (sf s＄ sx)         = sinj⊞ sf s＄ sinj⊞ sx
sinj⊞ s[ i ]              = s[ i ]
sinj⊞ (sLet sx se)        = sLet (sinj⊞ sx) (sinj⊞ se)
sinj⊞ (sLoop si sf)       = sLoop si (sinj⊞ sf)
sinj⊞ sUnit               = sUnit
sinj⊞ (sVal v)            = sVal v
sinj⊞ (sx s， sy)         = sinj⊞ sx s， sinj⊞ sy
sinj⊞ (sCase⊗ sxy sf)     = sCase⊗ (sinj⊞ sxy) (sinj⊞ sf)
sinj⊞ (sInl sx)           = sInl (sinj⊞ sx)
sinj⊞ (sInr sy)           = sInr (sinj⊞ sy)
sinj⊞ (sCase⊕ sxy sf sg)  = sCase⊕ (sinj⊞ sxy) (sinj⊞ sf) (sinj⊞ sg)
sinj⊞ sNil                = sNil
sinj⊞ (sCons sx sxs)      = sCons (sinj⊞ sx) (sinj⊞ sxs)
sinj⊞ (sHead sxs)         = sHead (sinj⊞ sxs)
sinj⊞ (sTail sxs)         = sTail (sinj⊞ sxs)
\end{code}
