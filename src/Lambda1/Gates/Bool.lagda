\begin{code}
module Lambda1.Gates.Bool where

open import Data.Bool.Base using (Bool)

open import Data.SimpleTypes using (U; ι; _⇒_)
open import Lambda1.Gates using (Gates)
\end{code}



%<*Bool-poly>
\AgdaTarget{𝔹}
\begin{code}
𝔹 : U Bool
𝔹 = ι
\end{code}
%</Bool-poly>




\begin{code}
data BOOLTRIOIx : Set where NOT AND OR XOR MUX2 : BOOLTRIOIx

BOOLTRIOty : BOOLTRIOIx → U Bool
BOOLTRIOty NOT   = 𝔹 ⇒ 𝔹
BOOLTRIOty AND   = 𝔹 ⇒ 𝔹 ⇒ 𝔹
BOOLTRIOty OR    = 𝔹 ⇒ 𝔹 ⇒ 𝔹
BOOLTRIOty XOR   = 𝔹 ⇒ 𝔹 ⇒ 𝔹
BOOLTRIOty MUX2  = 𝔹 ⇒ 𝔹 ⇒ 𝔹 ⇒ 𝔹

BOOLTRIO : Gates Bool
BOOLTRIO = record { gIx = BOOLTRIOIx;  gty = BOOLTRIOty }
\end{code}
