\begin{code}
module Lambda1.Gates.Nat where

open import Data.Nat.Base using (ℕ; _+_; _*_)

open import Data.SimpleTypes using (U; ι; _⇒_)
open import Lambda1.Gates using (Gates)
\end{code}



%<*Nat-poly>
\AgdaTarget{Nat-poly}
\begin{code}
Nat : U ℕ
Nat = ι
\end{code}
%</Nat-poly>



\begin{code}
data NATIx : Set where ADD MUL : NATIx

NATty : NATIx → U ℕ
NATty ADD = Nat ⇒ Nat ⇒ Nat
NATty MUL = Nat ⇒ Nat ⇒ Nat

NAT : Gates ℕ
NAT = record { gIx = NATIx; gty = NATty }
\end{code}
