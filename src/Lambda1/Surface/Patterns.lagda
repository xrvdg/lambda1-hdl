\begin{code}
module Lambda1.Surface.Patterns where

open import Function using (_∘′_)
open import Data.Nat.Base using (zero; suc)
open import Data.Vec using (Vec; []; _∷_; replicate; map)
open import Data.Star using (ε; _◅_)

open import Data.SimpleTypes using (𝟙; _⊗_; U; El; Ctxt; vec)
open import Lambda1.Lambda1 using (Unit)
open import Lambda1.BasicCombinators using (Replicate)
open import Lambda1.Surface.Lambda1 using (reIx₀; λH[_]; _,_; loop; fst; snd; nil; cons; mapAccL-par)
open import Lambda1.State using (λs; _s，_; sCase⊗; sLoop; sMapAccL-par)
open import Lambda1.Surface.State using (sReIx₀)
\end{code}




%<*mapper>
\AgdaTarget{mapper}
\begin{code}
mapper : ∀ {B G} {σ ρ τ : U B} (f : λH[ G ] ρ → λH[ G ] τ) (u : λH[ G ] σ) (x : λH[ G ] ρ) → λH[ G ] (σ ⊗ τ)
mapper f u x = u , f x
\end{code}
%</mapper>

%<*smapper>
\AgdaTarget{smapper}
\begin{code}
sMapper : ∀  {B G} {Γ : Ctxt B} {σ ρ τ} {f : λH[ G ] ρ → λH[ G ] τ} {u : λH[ G ] σ} {x : λH[ G ] ρ}
             (sfx : λs (f x)) (su : λs u) → λs {Γ = Γ} (mapper f u x)
sMapper sfx su = su s， sfx
\end{code}
%</smapper>


%<*iterator>
\AgdaTarget{iterator}
\begin{code}
iterator : ∀ {B G} {σ ρ : U B} (f : λH[ G ] σ → λH[ G ] σ) (u : λH[ G ] σ) (x : λH[_] {B} G ρ) → λH[ G ] (σ ⊗ σ)
iterator f u _ = u , f u
\end{code}
%</iterator>


%<*folder>
\AgdaTarget{folder}
\begin{code}
folder : ∀ {B G} {σ ρ : U B} (f : λH[ G ] σ → λH[ G ] ρ → λH[ G ] σ) (u : λH[ G ] σ) (x : λH[ G ] ρ) → λH[ G ] (σ ⊗ σ)
folder f u x = u , f u x
\end{code}
%</folder>





%<*smapAccL-par>
\AgdaTarget{smapAccL-par}
\begin{code}
smapAccL-par :  ∀  {n B G} {Γ : Ctxt B} {σ ρ τ} {f : λH[ G ] σ → λH[ G ] ρ → λH[ G ] (σ ⊗ τ)} {e : λH[ G ] σ} {xs : λH[ G ] (vec ρ n)}
                   (sf : (s : λH[ G ] σ) (r : λH[ G ] ρ) → λs {Γ = σ ◅ ρ ◅ Γ} (f s r)) (se : λs e) (sxs : λs xs)
                   → λs {Γ = Γ} (mapAccL-par f e xs)
smapAccL-par sf se sxs = sMapAccL-par (replicate (sf (reIx₀ _) (reIx₀ _))) se sxs
\end{code}
%</smapAccL-par>


%<*sloop>
\AgdaTarget{sloop}
\begin{code}
sloop :  ∀  {B G} {Γ : Ctxt B} {σ ρ τ} {f : λH[ G ] σ → λH[ G ] ρ → λH[ G ] (σ ⊗ τ)} {x : λH[ G ] ρ}
            (sf : (s : λH[ G ] σ) (r : λH[ G ] ρ) → λs {Γ = σ ◅ Γ} (f s r)) (e : El σ)
            → λs {Γ = Γ} (loop f x)
sloop {x = x} sf e = sLoop e (sf (reIx₀ _) x)
\end{code}
%</sloop>






%<*map-par>
\AgdaTarget{map-par}
\begin{code}
map-par : ∀ {n B G} {ρ τ : U B} (f : λH[ G ] ρ → λH[ G ] τ) (xs : λH[ G ] (vec ρ n)) → λH[ G ] (vec τ n)
map-par f = snd ∘′ mapAccL-par (mapper f) Unit
\end{code}
%</map-par>

smap-par : ∀ {n B} {Γ : Ctxt B} {ρ τ} {f : λH ρ → λH τ} (sf : λs {Γ = ρ ◅ Γ} (f (reIx₀ Γ))) (xs : Vec (El ρ) n) → λs {Γ = Γ} (map-par f (val xs))
smap-par {Γ = Γ} {ρ} {τ} {f} sf xs = sCase⊗ (sMapAccumL-par (replicate sMapper) sOne (sVal xs)) (sReIx₀ Γ)



%<*map-seq>
\AgdaTarget{map-seq}
\begin{code}
map-seq : ∀ {B G} {ρ τ : U B} (f : λH[ G ] ρ → λH[ G ] τ) (x : λH[ G ] ρ) → λH[ G ] τ
map-seq f = loop (mapper {σ = 𝟙} f)
\end{code}
%</map-seq>

smap-seq : ∀ {B} {Γ : Ctxt B} {ρ τ} {f : λH ρ → λH τ} (sf : λs {Γ = ρ ◅ Γ} (f (reIx₀ Γ))) (x : El ρ) → λs {Γ = Γ} (map-seq f (val x))
smap-seq {Γ = Γ} sf x = sMapAccumL-seq tt ((sReIx₀ Γ) s， ({!!}))




%<*iterate-par>
\AgdaTarget{iterate-par}
\begin{code}
iterate-par : ∀ {n B G} {τ : U B} (f : λH[ G ] τ → λH[ G ] τ) (x : λH[ G ] τ) → λH[ G ] (τ ⊗ vec τ n)
iterate-par f x = mapAccL-par (iterator f) x (Replicate Unit)
\end{code}
%</iterate-par>

%<*iterate-seq>
\AgdaTarget{iterate-seq}
\begin{code}
iterate-seq : ∀ {B G} {τ : U B} (f : λH[ G ] τ → λH[ G ] τ) → λH[ G ] τ
iterate-seq f = loop (iterator f) Unit
\end{code}
%</iterate-seq>


%<*repeat-par>
\AgdaTarget{repeat-par}
\begin{code}
repeat-par : ∀ {n B G} {τ : U B} (f : λH[ G ] τ → λH[ G ] τ) (x : λH[ G ] τ) → λH[ G ] τ
repeat-par {n} f = fst ∘′ iterate-par {n} f
\end{code}
%</repeat-par>

%<*repeat-seq>
\AgdaTarget{repeat-seq}
\begin{code}
repeat-seq : ∀ {B G} {τ : U B} (f : λH[ G ] τ → λH[ G ] τ) → λH[ G ] τ
repeat-seq = iterate-seq
\end{code}
%</repeat-seq>



%<*foldl-scanl-par>
\AgdaTarget{foldl-scanl-par}
\begin{code}
foldl-scanl-par : ∀ {n B G} {σ ρ : U B} (f : λH[ G ] σ → λH[ G ] ρ → λH[ G ] σ) (e : λH[ G ] σ) (xs : λH[ G ] (vec ρ n)) → λH[ G ] (σ ⊗ vec σ n)
foldl-scanl-par f = mapAccL-par (folder f)
\end{code}
%</foldl-scanl-par>

%<*foldl-scanl-seq>
\AgdaTarget{foldl-scanl-seq}
\begin{code}
foldl-scanl-seq : ∀ {B G} {σ ρ : U B} (f : λH[ G ] σ → λH[ G ] ρ → λH[ G ] σ) (x : λH[ G ] ρ) → λH[ G ] σ
foldl-scanl-seq f = loop (folder f)
\end{code}
%</foldl-scanl-seq>

%<*foldl-par>
\AgdaTarget{foldl-par}
\begin{code}
foldl-par : ∀ {n B G} {σ ρ : U B} (f : λH[ G ] σ → λH[ G ] ρ → λH[ G ] σ) (e : λH[ G ] σ) (xs : λH[ G ] (vec ρ n)) → λH[ G ] σ
foldl-par f e = fst ∘′ foldl-scanl-par f e
\end{code}
%</foldl-par>

%<*scanl-par>
\AgdaTarget{scanl-par}
\begin{code}
scanl-par : ∀ {n B G} {σ ρ : U B} (f : λH[ G ] σ → λH[ G ] ρ → λH[ G ] σ) (e : λH[ G ] σ) (xs : λH[ G ] (vec ρ n)) → λH[ G ] (vec σ n)
scanl-par f e = snd ∘′ foldl-scanl-par f e
\end{code}
%</scanl-par>




%<*triangle>
\AgdaTarget{triangle}
\begin{code}
triangle : ∀ {n B G} {τ : U B} (f : λH[ G ] τ → λH[ G ] τ) (x : λH[ G ] τ) → λH[ G ] (vec τ (suc n))
triangle {zero}    f x = cons x nil
triangle {suc n′}  f x = cons x (map-par f (triangle {n′} f x))
\end{code}
%</triangle>


-- Note: the mapAccL being applied is the "insertion" step in insertion sort
\begin{code}
\end{code}
insertionSort : ∀ {n B G} {τ : U B} (c : λH[ G ] τ → λH[ G ] τ → λH[ G ] (τ ⊗ τ)) (xs : λH[ G ] (vec τ n)) → λH[ G ] (vec τ n)
insertionSort {zero}    c _   = nil
insertionSort {suc n′}  c xs  = case[] xs of λ x xs′ → mapAccL-par n′ c x (insertionSort n′ c xs′)


---- Attempt (failed) to write seq. insertionSort. Loop can create NFAs, but L(insertionSort) is not regular.
---- Input stream example:  x ≡ 3 ∷ 7 ∷ 1 ∷ 2 ∷ 8 ∷ 3 ∷ 4 ∷ 9 ∷ 9 ...
---- Output stream example:  ⟦ insertionSort ⟧ x ≡ ... ∷ 1 ∷ 2 ∷ 3 ∷ 3 ∷ 4 ∷ 7 ∷ 8 ∷ 9 ∷ 9 ∷ ...
---- The PREFIX "..." in the output stream indicates that the smallest element among those presented
---- in a given prefix of the input CANNOT be determined BEFORE ALL THE ELEMENTS TO BE SORTED HAVE BEEN PRESENTED
---- AT THE INPUT.
---- CAN BE FIXED WITH AN UPPER BOUND ON INPUT SIZE
--%<*insertionSort-seq>
--\begin{code}
postulate insertionSort-seq : ∀ {B G} {τ : U B} (cmp : λH[ G ] τ → λH[ G ] τ → λH[ G ] (τ ⊗ τ)) (x : λH[ G ] τ) → λH[ G ] τ
--\end{code}
--%</insertionSort-seq>
