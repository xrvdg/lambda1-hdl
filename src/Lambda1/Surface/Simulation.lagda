\begin{code}
module Lambda1.Surface.Simulation where

open import Data.Product using (_×_)
open import Data.Vec using (Vec; replicate)
open import Data.Stream using (Stream; repeat)
open import Data.Star using (ε)

open import Data.SimpleTypes using (Ctxt; El; Env)
open import Lambda1.Gates using (Gates)
open import Lambda1.Surface.Lambda1 using (λH[_])
open import Lambda1.State using (λs)
open import Lambda1.Simulation using (Sim)
  renaming  ( ⟦_⊨_⟧s[_] to ⟬_⊨_⟭s[_]; ⟦_⊨_⟧S[_] to ⟬_⊨_⟭S[_]; ⟦_⊨_⟧n[_] to ⟬_⊨_⟭n[_]
            ; ⟦_⊨_⟧N[_] to ⟬_⊨_⟭N[_]; ⟦_⊨_⟧*[_] to ⟬_⊨_⟭*[_]; ⟦_⊨_⟧★[_] to ⟬_⊨_⟭★[_] )
\end{code}



\begin{code}
⟦_⊨_⟧s[_]  : ∀ {B G} {Γ : Ctxt B} {τ} (S : Sim G) (c : λH[ G ] τ)  (s : λs {B} c)       (γ : Env El Γ)            → (λs c × El τ)
⟦_⊨_⟧n[_]  : ∀ {B G} {Γ : Ctxt B} {τ} (S : Sim G) (c : λH[ G ] τ)  (s : λs {B} c)    n  (γn : Vec (Env El Γ) n)   → (λs c × Vec (El τ) n)
⟦_⊨_⟧*[_]  : ∀ {B G} {Γ : Ctxt B} {τ} (S : Sim G) (c : λH[ G ] τ)  (s : λs {B} c)       (γ* : Stream (Env El Γ))  → (Stream (λs c) × Stream (El τ))
⟦_⊨_⟧S[_]  : ∀ {B G} {Γ : Ctxt B} {τ} (S : Sim G) (c : λH[ G ] τ)  (s : λs {B} c)       (γ : Env El Γ)            → El τ
⟦_⊨_⟧N[_]  : ∀ {B G} {Γ : Ctxt B} {τ} (S : Sim G) (c : λH[ G ] τ)  (s : λs {B} c)    n  (γn : Vec (Env El Γ) n)   → Vec (El τ) n
⟦_⊨_⟧★[_]  : ∀ {B G} {Γ : Ctxt B} {τ} (S : Sim G) (c : λH[ G ] τ)  (s : λs {B} c)       (γ* : Stream (Env El Γ))  → Stream (El τ)
⟦_⊨_⟧s     : ∀ {B G} {Γ : Ctxt B} {τ} (S : Sim G) (c : λH[ G ] τ)  ⦃ s : λs {B} c ⦄     (γ : Env El Γ)            → (λs c × El τ)
⟦_⊨_⟧n     : ∀ {B G} {Γ : Ctxt B} {τ} (S : Sim G) (c : λH[ G ] τ)  ⦃ s : λs {B} c ⦄  n  (γn : Vec (Env El Γ) n)   → (λs c × Vec (El τ) n)
⟦_⊨_⟧*     : ∀ {B G} {Γ : Ctxt B} {τ} (S : Sim G) (c : λH[ G ] τ)  ⦃ s : λs {B} c ⦄     (γ* : Stream (Env El Γ))  → (Stream (λs c) × Stream (El τ))
⟦_⊨_⟧S     : ∀ {B G} {Γ : Ctxt B} {τ} (S : Sim G) (c : λH[ G ] τ)  ⦃ s : λs {B} c ⦄     (γ : Env El Γ)            → El τ
⟦_⊨_⟧N     : ∀ {B G} {Γ : Ctxt B} {τ} (S : Sim G) (c : λH[ G ] τ)  ⦃ s : λs {B} c ⦄  n  (γn : Vec (Env El Γ) n)   → Vec (El τ) n
⟦_⊨_⟧★     : ∀ {B G} {Γ : Ctxt B} {τ} (S : Sim G) (c : λH[ G ] τ)  ⦃ s : λs {B} c ⦄     (γ* : Stream (Env El Γ))  → Stream (El τ)

⟦_⊨_⟧ś[_]   : ∀ {B G τ} (S : Sim G) (c : λH[ G ] τ)  (s : λs {B} c)       → (λs c × El τ)
⟦_⊨_⟧ń[_]   : ∀ {B G τ} (S : Sim G) (c : λH[ G ] τ)  (s : λs {B} c)    n  → (λs c × Vec (El τ) n)
⟦_⊨_⟧*'[_]  : ∀ {B G τ} (S : Sim G) (c : λH[ G ] τ)  (s : λs {B} c)       → (Stream (λs c) × Stream (El τ))
⟦_⊨_⟧Ś[_]   : ∀ {B G τ} (S : Sim G) (c : λH[ G ] τ)  (s : λs {B} c)       → El τ
⟦_⊨_⟧Ń[_]   : ∀ {B G τ} (S : Sim G) (c : λH[ G ] τ)  (s : λs {B} c)    n  → Vec (El τ) n
⟦_⊨_⟧★'[_]  : ∀ {B G τ} (S : Sim G) (c : λH[ G ] τ)  (s : λs {B} c)       → Stream (El τ)
⟦_⊨_⟧ś      : ∀ {B G τ} (S : Sim G) (c : λH[ G ] τ)  ⦃ s : λs {B} c ⦄     → (λs c × El τ)
⟦_⊨_⟧ń      : ∀ {B G τ} (S : Sim G) (c : λH[ G ] τ)  ⦃ s : λs {B} c ⦄  n  → (λs c × Vec (El τ) n)
⟦_⊨_⟧*'     : ∀ {B G τ} (S : Sim G) (c : λH[ G ] τ)  ⦃ s : λs {B} c ⦄     → (Stream (λs c) × Stream (El τ))
⟦_⊨_⟧Ś      : ∀ {B G τ} (S : Sim G) (c : λH[ G ] τ)  ⦃ s : λs {B} c ⦄     → El τ
⟦_⊨_⟧Ń      : ∀ {B G τ} (S : Sim G) (c : λH[ G ] τ)  ⦃ s : λs {B} c ⦄  n  → Vec (El τ) n
⟦_⊨_⟧★'     : ∀ {B G τ} (S : Sim G) (c : λH[ G ] τ)  ⦃ s : λs {B} c ⦄     → Stream (El τ)
\end{code}



%<*simopen-step-state-explicit>
\AgdaTarget{⟦\_⊨\_⟧s[\_]}
\begin{code}
⟦ S ⊨ c ⟧s[ s ] = ⟬ S ⊨ c ⟭s[ s ]
\end{code}
%</simopen-step-state-explicit>

%<*simopen-step-state-instance>
\AgdaTarget{⟦\_⊨\_⟧s}
\begin{code}
⟦ S ⊨ c ⟧s ⦃ s ⦄ = ⟦ S ⊨ c ⟧s[ s ]
\end{code}
%</simopen-step-state-instance>


%<*simopen-step-nostate-explicit>
\AgdaTarget{⟦\_⊨\_⟧S[\_]}
\begin{code}
⟦ S ⊨ c ⟧S[ s ] = ⟬ S ⊨ c ⟭S[ s ]
\end{code}
%</simopen-step-nostate-explicit>

%<*simopen-step-nostate-instance>
\AgdaTarget{⟦\_⊨\_⟧S}
\begin{code}
⟦ S ⊨ c ⟧S ⦃ s ⦄ = ⟦ S ⊨ c ⟧S[ s ]
\end{code}
%</simopen-step-nostate-instance>


%<*simopen-vec-state-explicit>
\AgdaTarget{⟦\_⊨\_⟧n[\_]}
\begin{code}
⟦ S ⊨ c ⟧n[ s ] = ⟬ S ⊨ c ⟭n[ s ]
\end{code}
%</simopen-vec-state-explicit>

%<*simopen-vec-state-instance>
\AgdaTarget{⟦\_⊨\_⟧n}
\begin{code}
⟦ S ⊨ c ⟧n ⦃ s ⦄ = ⟦ S ⊨ c ⟧n[ s ]
\end{code}
%</simopen-vec-state-instance>


%<*simopen-vec-nostate-explicit>
\AgdaTarget{⟦\_⊨\_⟧N[\_]}
\begin{code}
⟦ S ⊨ c ⟧N[ s ] = ⟬ S ⊨ c ⟭N[ s ]
\end{code}
%</simopen-vec-nostate-explicit>

%<*simopen-vec-nostate-instance>
\AgdaTarget{⟦\_⊨\_⟧N}
\begin{code}
⟦ S ⊨ c ⟧N ⦃ s ⦄ = ⟦ S ⊨ c ⟧N[ s ]
\end{code}
%</simopen-vec-nostate-instance>


%<*simopen-stream-state-explicit>
\AgdaTarget{⟦\_⊨\_⟧*[\_]}
\begin{code}
⟦ S ⊨ c ⟧*[ s ] = ⟬ S ⊨ c ⟭*[ s ]
\end{code}
%</simopen-stream-state-explicit>

%<*simopen-stream-state-instance>
\AgdaTarget{⟦\_⊨\_⟧*}
\begin{code}
⟦ S ⊨ c ⟧* ⦃ s ⦄ = ⟦ S ⊨ c ⟧*[ s ]
\end{code}
%</simopen-stream-state-instance>


%<*simopen-stream-nostate-explicit>
\AgdaTarget{⟦\_⊨\_⟧★[\_]}
\begin{code}
⟦ S ⊨ c ⟧★[ s ] = ⟬ S ⊨ c ⟭★[ s ]
\end{code}
%</simopen-stream-nostate-explicit>

%<*simopen-stream-nostate-instance>
\AgdaTarget{⟦\_⊨\_⟧★}
\begin{code}
⟦ S ⊨ c ⟧★ ⦃ s ⦄ = ⟦ S ⊨ c ⟧★[ s ]
\end{code}
%</simopen-stream-nostate-instance>






%<*simclosed-step-state-explicit>
\AgdaTarget{⟦\_⊨\_⟧ś[\_]}
\begin{code}
⟦ S ⊨ c ⟧ś[ s ] = ⟦ S ⊨ c ⟧s[ s ] ε
\end{code}
%</simclosed-step-state-explicit>

%<*simclosed-step-state-instance>
\AgdaTarget{⟦\_⊨\_⟧ś}
\begin{code}
⟦ S ⊨ c ⟧ś ⦃ s ⦄ = ⟦ S ⊨ c ⟧ś[ s ]
\end{code}
%</simclosed-step-state-instance>

%<*simclosed-step-nostate-explicit>
\AgdaTarget{⟦\_⊨\_⟧Ś[\_]}
\begin{code}
⟦ S ⊨ c ⟧Ś[ s ] = ⟦ S ⊨ c ⟧S[ s ] ε
\end{code}
%</simclosed-step-nostate-explicit>

%<*simclosed-step-nostate-instance>
\AgdaTarget{⟦\_⊨\_⟧Ś}
\begin{code}
⟦ S ⊨ c ⟧Ś ⦃ s ⦄ = ⟦ S ⊨ c ⟧Ś[ s ]
\end{code}
%</simclosed-step-nostate-instance>


%<*simclosed-vec-state-explicit>
\AgdaTarget{⟦\_⊨\_⟧ń[\_]}
\begin{code}
⟦ S ⊨ c ⟧ń[ s ] n = ⟦ S ⊨ c ⟧n[ s ] n (replicate ε)
\end{code}
%</simclosed-vec-state-explicit>

%<*simclosed-vec-state-instance>
\AgdaTarget{⟦\_⊨\_⟧ń}
\begin{code}
⟦ S ⊨ c ⟧ń ⦃ s ⦄ = ⟦ S ⊨ c ⟧ń[ s ]
\end{code}
%</simclosed-vec-state-instance>

%<*simclosed-vec-nostate-explicit>
\AgdaTarget{⟦\_⊨\_⟧Ń[\_]}
\begin{code}
⟦ S ⊨ c ⟧Ń[ s ] n = ⟦ S ⊨ c ⟧N[ s ] n (replicate ε)
\end{code}
%</simclosed-vec-nostate-explicit>

%<*simclosed-vec-nostate-instance>
\AgdaTarget{⟦\_⊨\_⟧Ń}
\begin{code}
⟦ S ⊨ c ⟧Ń ⦃ s ⦄ = ⟦ S ⊨ c ⟧Ń[ s ]
\end{code}
%</simclosed-vec-nostate-instance>


%<*simclosed-stream-state-explicit>
\AgdaTarget{⟦\_⊨\_⟧*'[\_]}
\begin{code}
⟦ S ⊨ c ⟧*'[ s ] = ⟦ S ⊨ c ⟧*[ s ] (repeat ε)
\end{code}
%</simclosed-stream-state-explicit>

%<*simclosed-stream-state-instance>
\AgdaTarget{⟦\_⊨\_⟧*'}
\begin{code}
⟦ S ⊨ c ⟧*' ⦃ s ⦄ = ⟦ S ⊨ c ⟧*'[ s ]
\end{code}
%</simclosed-stream-state-instance>

%<*simclosed-stream-nostate-explicit>
\AgdaTarget{⟦\_⊨\_⟧★'[\_]}
\begin{code}
⟦ S ⊨ c ⟧★'[ s ] = ⟦ S ⊨ c ⟧★[ s ] (repeat ε)
\end{code}
%</simclosed-stream-nostate-explicit>

%<*simclosed-stream-nostate-instance>
\AgdaTarget{⟦\_⊨\_⟧★'}
\begin{code}
⟦ S ⊨ c ⟧★' ⦃ s ⦄ = ⟦ S ⊨ c ⟧★'[ s ]
\end{code}
%</simclosed-stream-nostate-instance>




\begin{code}
module SimulationSurfaceWithGateSim {B} {G : Gates B} (S : Sim G) where
\end{code}

\begin{code}
 ⟦_⟧s[_]  : ∀ {Γ τ} (c : λH[ G ] τ)   (s : λs {Γ = Γ} c)      (γ : Env El Γ)            → (λs {Γ = Γ} c × El τ)
 ⟦_⟧n[_]  : ∀ {Γ τ} (c : λH[ G ] τ)   (s : λs {Γ = Γ} c) n    (γn : Vec (Env El Γ) n)   → (λs {Γ = Γ} c × Vec (El τ) n)
 ⟦_⟧*[_]  : ∀ {Γ τ} (c : λH[ G ] τ)   (s : λs {Γ = Γ} c)      (γ* : Stream (Env El Γ))  → (Stream (λs {Γ = Γ} c) × Stream (El τ))
 ⟦_⟧S[_]  : ∀ {Γ τ} (c : λH[ G ] τ)   (s : λs {Γ = Γ} c)      (γ : Env El Γ)            → El τ
 ⟦_⟧N[_]  : ∀ {Γ τ} (c : λH[ G ] τ)   (s : λs {Γ = Γ} c) n    (γn : Vec (Env El Γ) n)   → Vec (El τ) n
 ⟦_⟧★[_]  : ∀ {Γ τ} (c : λH[ G ] τ)   (s : λs {Γ = Γ} c)      (γ* : Stream (Env El Γ))  → Stream (El τ)
 ⟦_⟧s     : ∀ {Γ τ} (c : λH[ G ] τ)   ⦃ s : λs {Γ = Γ} c ⦄    (γ : Env El Γ)            → (λs {Γ = Γ} c × El τ)
 ⟦_⟧n     : ∀ {Γ τ} (c : λH[ G ] τ)   ⦃ s : λs {Γ = Γ} c ⦄ n  (γn : Vec (Env El Γ) n)   → (λs {Γ = Γ} c × Vec (El τ) n)
 ⟦_⟧*     : ∀ {Γ τ} (c : λH[ G ] τ)   ⦃ s : λs {Γ = Γ} c ⦄    (γ* : Stream (Env El Γ))  → (Stream (λs {Γ = Γ} c) × Stream (El τ))
 ⟦_⟧S     : ∀ {Γ τ} (c : λH[ G ] τ)   ⦃ s : λs {Γ = Γ} c ⦄    (γ : Env El Γ)            → El τ
 ⟦_⟧N     : ∀ {Γ τ} (c : λH[ G ] τ)   ⦃ s : λs {Γ = Γ} c ⦄ n  (γn : Vec (Env El Γ) n)   → Vec (El τ) n
 ⟦_⟧★     : ∀ {Γ τ} (c : λH[ G ] τ)   ⦃ s : λs {Γ = Γ} c ⦄    (γ* : Stream (Env El Γ))  → Stream (El τ)

 ⟦_⟧ś[_]   : ∀ {τ} (c : λH[ G ] τ)   (s : λs {Γ = ε} c)      → (λs {Γ = ε} c × El τ)
 ⟦_⟧ń[_]   : ∀ {τ} (c : λH[ G ] τ)   (s : λs {Γ = ε} c) n    → (λs {Γ = ε} c × Vec (El τ) n)
 ⟦_⟧*'[_]  : ∀ {τ} (c : λH[ G ] τ)   (s : λs {Γ = ε} c)      → (Stream (λs {Γ = ε} c) × Stream (El τ))
 ⟦_⟧Ś[_]   : ∀ {τ} (c : λH[ G ] τ)   (s : λs {Γ = ε} c)      → El τ
 ⟦_⟧Ń[_]   : ∀ {τ} (c : λH[ G ] τ)   (s : λs {Γ = ε} c) n    → Vec (El τ) n
 ⟦_⟧★'[_]  : ∀ {τ} (c : λH[ G ] τ)   (s : λs {Γ = ε} c)      → Stream (El τ)
 ⟦_⟧ś      : ∀ {τ} (c : λH[ G ] τ)   ⦃ s : λs {Γ = ε} c ⦄    → (λs {Γ = ε} c × El τ)
 ⟦_⟧ń      : ∀ {τ} (c : λH[ G ] τ)   ⦃ s : λs {Γ = ε} c ⦄ n  → (λs {Γ = ε} c × Vec (El τ) n)
 ⟦_⟧*'     : ∀ {τ} (c : λH[ G ] τ)   ⦃ s : λs {Γ = ε} c ⦄    → (Stream (λs {Γ = ε} c) × Stream (El τ))
 ⟦_⟧Ś      : ∀ {τ} (c : λH[ G ] τ)   ⦃ s : λs {Γ = ε} c ⦄    → El τ
 ⟦_⟧Ń      : ∀ {τ} (c : λH[ G ] τ)   ⦃ s : λs {Γ = ε} c ⦄ n  → Vec (El τ) n
 ⟦_⟧★'     : ∀ {τ} (c : λH[ G ] τ)   ⦃ s : λs {Γ = ε} c ⦄    → Stream (El τ)
\end{code}

\begin{code}
 ⟦_⟧s[_] = ⟦ S ⊨_⟧s[_]
 ⟦_⟧n[_] = ⟦ S ⊨_⟧n[_]
 ⟦_⟧*[_] = ⟦ S ⊨_⟧*[_]
 ⟦_⟧S[_] = ⟦ S ⊨_⟧S[_]
 ⟦_⟧N[_] = ⟦ S ⊨_⟧N[_]
 ⟦_⟧★[_] = ⟦ S ⊨_⟧★[_]
 ⟦_⟧s = ⟦ S ⊨_⟧s
 ⟦_⟧n = ⟦ S ⊨_⟧n
 ⟦_⟧* = ⟦ S ⊨_⟧*
 ⟦_⟧S = ⟦ S ⊨_⟧S
 ⟦_⟧N = ⟦ S ⊨_⟧N
 ⟦_⟧★ = ⟦ S ⊨_⟧★

 ⟦_⟧ś[_]   = ⟦ S ⊨_⟧ś[_]
 ⟦_⟧ń[_]   = ⟦ S ⊨_⟧ń[_]
 ⟦_⟧*'[_]  = ⟦ S ⊨_⟧*'[_]
 ⟦_⟧Ś[_]   = ⟦ S ⊨_⟧Ś[_]
 ⟦_⟧Ń[_]   = ⟦ S ⊨_⟧Ń[_]
 ⟦_⟧★'[_]  = ⟦ S ⊨_⟧★'[_]
 ⟦_⟧ś   = ⟦ S ⊨_⟧ś
 ⟦_⟧ń   = ⟦ S ⊨_⟧ń
 ⟦_⟧*'  = ⟦ S ⊨_⟧*'
 ⟦_⟧Ś   = ⟦ S ⊨_⟧Ś
 ⟦_⟧Ń   = ⟦ S ⊨_⟧Ń
 ⟦_⟧★'  = ⟦ S ⊨_⟧★'
\end{code}
