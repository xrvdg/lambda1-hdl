\begin{code}
module Lambda1.Surface.Lambda1 where

open import Data.Unit.Base using (tt)
open import Data.Sum using (inj₁)
open import Data.Nat.Base using (zero; suc)
open import Data.Star using (_◅_)

open import Data.SimpleTypes using (U; El; 𝟙; _⊗_; _⊕_; vec; Ctxt; reIx₀∋)
open import Lambda1.Gates using (Gates; _⊞_)
open import Lambda1.Lambda1 public
\end{code}



* Assumes Δ ⊒ (τ ◅ Γ). Stuck in postulate otherwise
%<*reIx0>
\AgdaTarget{reIx₀}
\begin{code}
reIx₀ : ∀ {B} {G : Gates B} {τ} (Γ {Δ} : Ctxt B) → λB[ G ] Δ τ
reIx₀ {B} {τ = τ} Γ {Δ} = [ reIx₀∋ {B} τ Γ Δ ]
\end{code}
%</reIx0>


%<*LambdaH>
\AgdaTarget{λH[\_]}
\begin{code}
λH[_] : ∀ {B} (G : Gates B) (τ : U B) → Set
λH[_] {B} G τ = ∀ {Γ : Ctxt B} → λB[ G ] Γ τ
\end{code}
%</LambdaH>


%<*let′>
\AgdaTarget{let′}
\begin{code}
let′ : ∀ {B} {G : Gates B} {σ τ} (x : λH[ G ] σ) (f : λH[ G ] σ → λH[ G ] τ) → λH[_] {B} G τ
(let′ x f) {Γ} = Let x (f (reIx₀ Γ))
\end{code}
%</let′>

%<*let′>
\begin{code}
syntax let′ e (λ x → b) = let′ x ≔ e in′ b
infixr 1 let′
\end{code}
%</let′>


%<*loop>
\AgdaTarget{loop}
\begin{code}
loop : ∀ {B} {G : Gates B} {σ ρ τ : U B} (f : λH[ G ] σ → λH[ G ] ρ → λH[ G ] (σ ⊗ τ)) (x : λH[ G ] ρ) → λH[ G ] τ
(loop f x) {Γ} = Loop (f (reIx₀ Γ) x)
\end{code}
%</loop>

%<*loop-notation>
\begin{code}
syntax loop {σ = σ} (λ s → b) = loop s ∶ σ in′ b
infixr 1 loop
\end{code}
%</loop-notation>

%<*loop-out-next>
\AgdaTarget{loop-out-next}
\begin{code}
loop-out-next : ∀ {B} {G : Gates B} {σ τ} (fo : λH[ G ] σ → λH[ G ] τ) (fn : λH[ G ] σ → λH[ G ] σ) → λH[_] {B} G τ
(loop-out-next fo fn) {Γ} = Loop-out-next (fo (reIx₀ Γ)) (fn (reIx₀ Γ))
\end{code}
%</loop-out-next>

-- NOTE: s₁ and s₂ are different names for the SAME value, Agda mixfix binding feature requires this.
%<*loop-out-next-notation>
\begin{code}
syntax loop-out-next {σ = σ} (λ s₁ → o) (λ s₂ → n) = loop s₁ type s₂ ∶ σ  out o next n
infixr 1 loop-out-next
\end{code}
%</loop-out-next-notation>


%<*pair>
\AgdaTarget{\_,\_}
\begin{code}
_,_ : ∀ {B} {G : Gates B} {τ υ} (x : λH[ G ] τ) (y : λH[ G ] υ) → λH[_] {B} G (τ ⊗ υ)
(x , y) = x ， y
\end{code}
%</pair>

%<*case-prod>
\AgdaTarget{case⊗\_of\_}
\begin{code}
case⊗_of_ : ∀ {B} {G : Gates B} {σ ρ τ} (xy : λH[ G ] (σ ⊗ ρ)) (f : λH[ G ] σ → λH[ G ] ρ → λH[ G ] τ) → λH[_] {B} G τ
(case⊗_of_ {ρ = ρ} xy f) {Γ} = Case⊗ xy of f (reIx₀ (ρ ◅ Γ)) (reIx₀ Γ)
\end{code}
%</case-prod>


%<*inl>
\AgdaTarget{inl}
\begin{code}
inl : ∀ {B} {G : Gates B} {τ υ} (x : λH[ G ] τ) → λH[_] {B} G (τ ⊕ υ)
inl x = Inl x
\end{code}
%</inl>

%<*inr>
\AgdaTarget{inr}
\begin{code}
inr : ∀ {B} {G : Gates B} {τ υ} (y : λH[ G ] υ) → λH[_] {B} G (τ ⊕ υ)
inr y = Inr y
\end{code}
%</inr>

%<*case-coprod>
\AgdaTarget{case⊕\_of\_or\_}
\begin{code}
case⊕_of_or_ : ∀ {B} {G : Gates B} {σ ρ τ} (xy : λH[ G ] (σ ⊕ ρ)) (f : λH[ G ] σ → λH[ G ] τ) (g : λH[ G ] ρ → λH[ G ] τ) → λH[_] {B} G τ
(case⊕ xy of f or g) {Γ} = Case⊕ xy of f (reIx₀ Γ) or g (reIx₀ Γ)
\end{code}
%</case-coprod>


%<*nil>
\AgdaTarget{nil}
\begin{code}
nil : ∀ {B} {G : Gates B} {τ} → λH[_] {B} G (vec τ zero)
nil = Nil
\end{code}
%</nil>

%<*cons>
\AgdaTarget{cons}
\begin{code}
cons : ∀ {n B} {G : Gates B} {τ} (x : λH[ G ] τ) (xs : λH[ G ] (vec τ n)) → λH[_] {B} G (vec τ (suc n))
cons x xs = Cons x xs
\end{code}
%</cons>

%<*mapAccL-par>
\AgdaTarget{mapAccumL-par}
\begin{code}
mapAccL-par : ∀ {n B} {G : Gates B} {σ ρ τ} (f : λH[ G ] σ → λH[ G ] ρ → λH[ G ] (σ ⊗ τ)) (e : λH[ G ] σ) (xs : λH[_] {B} G (vec ρ n)) → λH[_] {B} G (σ ⊗ vec τ n)
(mapAccL-par {ρ = ρ} f e xs) {Γ} = MapAccL-par (f (reIx₀ (ρ ◅ Γ)) (reIx₀ Γ)) e xs
\end{code}
%</mapAccumL-par>




%<*fork>
\AgdaTarget{fork}
\begin{code}
fork : ∀ {B} {G : Gates B} {τ} (x : λH[ G ] τ) → λH[_] {B} G (τ ⊗ τ)
fork x = let′ y ≔ x in′ (y , y)
\end{code}
%</fork>


%<*fst>
\AgdaTarget{fst}
\begin{code}
fst : ∀ {B} {G : Gates B} {τ υ} (xy : λH[ G ] (τ ⊗ υ)) → λH[_] {B} G τ
fst xy = case⊗ xy of (λ x _ → x)
\end{code}
%</fst>

%<*snd>
\AgdaTarget{snd}
\begin{code}
snd : ∀ {B} {G : Gates B} {τ υ} (xy : λH[ G ] (τ ⊗ υ)) → λH[_] {B} G υ
snd xy =  case⊗ xy of (λ _ y → y)
\end{code}
%</snd>


-- TODO: internalize this?
%<*replicate>
\AgdaTarget{replicate}
\begin{code}
replicate : ∀ {n B} {G : Gates B} {τ : U B} (x : λH[ G ] τ) → λH[ G ] (vec τ n)
replicate {zero}   _ = nil
replicate {suc _}  x = cons x (replicate x)
\end{code}
%</replicate>


units : ∀ {n B} → λH[_] {B} UNIT (vec 𝟙 n)
units = replicate unit
%<*units>
\AgdaTarget{units}
\begin{code}
\end{code}
%</ones>




\begin{code}
module Lambda1SurfaceWithGates {B} (G : Gates B) where
\end{code}

\begin{code}
 λh : ∀ (τ : U B) → Set
 λh τ = λH[ G ] τ
\end{code}
