\begin{code}
module Lambda1.Surface.State where

open import Data.SimpleTypes using (Ctxt; reIx₀∋)
open import Lambda1.Surface.Lambda1 using (reIx₀)
import Lambda1.State as Core
open Core using (s[_])
\end{code}


\begin{code}
open Core using (λs; ds) public
\end{code}


* Assumes Δ ⊒ (τ ◅ Γ). Stuck in postulate otherwise
%<*sReIx0>
\AgdaTarget{sReIx₀}
\begin{code}
sReIx₀ : ∀ {B G τ} (Γ {Δ} : Ctxt B) → λs {G = G} {Δ} (reIx₀ Γ)
sReIx₀ {τ = τ} Γ {Δ} = s[ reIx₀∋ τ Γ Δ ]
\end{code}
%</sReIx0>
