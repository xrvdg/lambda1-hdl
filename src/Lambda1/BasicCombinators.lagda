\begin{code}
module Lambda1.BasicCombinators where

open import Data.Nat.Base using (zero; suc)
open import Data.Unit.Base using (tt)
open import Data.Sum using (inj₁)
open import Data.Star using (_◅_)

open import Data.SimpleTypes using (Ctxt; _⊇_; flip⊇; refl⊇; El; 𝟙; vec; _⊗_)
open import Lambda1.Gates using (_⊞_)
open import Lambda1.Lambda1 using (λB[_]; K; ⟨_⟩; Unit; Nil; Cons; Case⊗_of_; #₀; #₁)
open import Lambda1.State using (λs; s⟨_⟩; sUnit; sCase⊗; sNil; sCons; s#₀; s#₁; sK)
\end{code}



%<*Flip′>
\AgdaTarget{Flip′}
\begin{code}
Flip′ : ∀ {B G} {Γ Δ : Ctxt B} {σ ρ τ} (c : λB[ G ] (σ ◅ ρ ◅ Γ) τ) {p : Δ ⊇ Γ} → λB[ G ] (ρ ◅ σ ◅ Δ) τ
Flip′ c {p} = K c {flip⊇ p}
\end{code}
%</Flip′>

%<*sFlip′>
\AgdaTarget{sFlip′}
\begin{code}
sFlip′ : ∀ {B G} {Γ Δ : Ctxt B} {σ ρ τ} {c : λB[ G ] (σ ◅ ρ ◅ Γ) τ} (s : λs c) {p : Δ ⊇ Γ} → λs (Flip′ c {p})
sFlip′ s {p} = sK s {flip⊇ p}
\end{code}
%</sFlip′>


%<*Flip>
\AgdaTarget{Flip}
\begin{code}
Flip : ∀ {B G} {Γ : Ctxt B} {σ ρ τ} (c : λB[ G ] (σ ◅ ρ ◅ Γ) τ) → λB[ G ] (ρ ◅ σ ◅ Γ) τ
Flip c = Flip′ c {refl⊇}
\end{code}
%</Flip>

%<*sFlip>
\AgdaTarget{sFlip}
\begin{code}
sFlip : ∀ {B G} {Γ : Ctxt B} {σ ρ τ} {c : λB[ G ] (σ ◅ ρ ◅ Γ) τ} (s : λs c) → λs (Flip c)
sFlip s = sFlip′ s {refl⊇}
\end{code}
%</sFlip>



%<*Replicate>
\AgdaTarget{Replicate}
\begin{code}
Replicate : ∀ {n B G} {Γ : Ctxt B} {τ} (x : λB[ G ] Γ τ) → λB[ G ] Γ (vec τ n)
Replicate {zero}   _ = Nil
Replicate {suc _}  x = Cons x (Replicate x)
\end{code}
%</Replicate>

%<*sReplicate>
\AgdaTarget{sReplicate}
\begin{code}
sReplicate : ∀ {n B G} {Γ : Ctxt B} {τ} {x : λB[ G ] Γ τ} (s : λs x) → λs (Replicate {n} x)
sReplicate {zero}   _ = sNil
sReplicate {suc _}  s = sCons s (sReplicate s)
\end{code}
%</sReplicate>


%<*Units>
\AgdaTarget{Units}
\begin{code}
Units : ∀ {n B G} {Γ : Ctxt B} → λB[ G ] Γ (vec 𝟙 n)
Units = Replicate Unit
\end{code}
%</Units>

%<*sUnits>
\AgdaTarget{sUnits}
\begin{code}
sUnits : ∀ {n B G} {Γ : Ctxt B} → λs (Units {n} {G = G} {Γ})
sUnits {n} = sReplicate {n} sUnit
\end{code}
%</sUnits>


%<*Fst>
\AgdaTarget{Fst}
\begin{code}
Fst : ∀ {B G} {Γ : Ctxt B} {τ υ} (c : λB[ G ] Γ (τ ⊗ υ)) → λB[ G ] Γ τ
Fst c = Case⊗ c of #₀
\end{code}
%</Fst>

%<*sFst>
\AgdaTarget{sFst}
\begin{code}
sFst : ∀ {B G} {Γ : Ctxt B} {τ υ} {c : λB[ G ] Γ (τ ⊗ υ)} (s : λs c) → λs (Fst c)
sFst s = sCase⊗ s s#₀
\end{code}
%</sFst>


%<*Snd>
\AgdaTarget{Snd}
\begin{code}
Snd : ∀ {B G} {Γ : Ctxt B} {τ υ} (c : λB[ G ] Γ (τ ⊗ υ)) → λB[ G ] Γ υ
Snd c = Case⊗ c of #₁
\end{code}
%</Snd>

%<*sSnd>
\AgdaTarget{sSnd}
\begin{code}
sSnd : ∀ {B G} {Γ : Ctxt B} {τ υ} {c : λB[ G ] Γ (τ ⊗ υ)} (s : λs c) → λs (Snd c)
sSnd s = sCase⊗ s s#₁
\end{code}
%</sSnd>

