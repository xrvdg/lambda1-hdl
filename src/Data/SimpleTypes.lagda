\begin{code}
module Data.SimpleTypes where

import Data.Star.Environment as Environment
open import Function using (id)
open import Data.List using (List; []; _∷_)
open import Function using (const)
open import Data.Empty using (⊥)
open import Data.Nat.Base using (ℕ; zero; suc; _∸_)
open import Data.Unit.Base using (tt; ⊤)
open import Data.Product using (_×_; _,_) renaming (map to map×)
open import Data.Sum using (_⊎_; inj₁)
open import Data.Vec using (Vec; replicate)
open import Data.Star using (ε; _◅_)
open import Data.Star.Decoration using (↦)
open import Data.Star.Pointer using (step; done)

open import Relation.Binary.PropositionalEquality using (_≡_; refl)
open import Relation.Binary.PropositionalEquality.TrustMe using (trustMe)
\end{code}


\begin{code}
infixr 0 _⇒_
infixr 1 _⊕_
infixr 2 _⊗_
\end{code}

* First, we define inductively the collection of all possible types for circuit inputs/outputs
%<*U>
\AgdaTarget{U}
\begin{code}
data U (B : Set) : Set where
  𝟙    : U B
  ι    : U B
  _⇒_  : (σ τ : U B) → U B
  _⊗_  : (τ υ : U B) → U B
  _⊕_  : (τ υ : U B) → U B
  vec  : (τ : U B) (n : ℕ) → U B
\end{code}
%</U>


%<*ord0>
\AgdaTarget{ord0}
\begin{code}
ord0 : ∀ {B} (τ : U B) → Set
ord0 𝟙          = ⊤
ord0 ι          = ⊤  -- Assuming that base types must be zeroth-order
ord0 (_ ⇒ _)    = ⊥
ord0 (τ ⊗ υ)    = ord0 τ × ord0 υ
ord0 (τ ⊕ υ)    = ord0 τ × ord0 υ
ord0 (vec τ _)  = ord0 τ
\end{code}
%</ord0>

%<*ord>
\AgdaTarget{ord}
\begin{code}
ord : ∀ {B} (n : ℕ) (τ : U B) → Set
ord zero         τ          = ord0 τ
ord (suc _)      ι          = ⊤
ord (suc _)      𝟙          = ⊤
ord n@(suc n-1)  (σ ⇒ τ)    = ord n-1 σ  × ord n τ
ord n@(suc _)    (τ ⊗ υ)    = ord n τ    × ord n υ
ord n@(suc _)    (τ ⊕ υ)    = ord n τ    × ord n υ
ord n@(suc _)    (vec τ _)  = ord n τ
\end{code}
%</ord>


%<*uncurryU>
\AgdaTarget{uncurryU}
\begin{code}
uncurryU : ∀ {B} (τ : U B) → List (U B) × (U B)
uncurryU (σ ⇒ τ)  = map× (σ ∷_) id (uncurryU τ)
uncurryU τ        = [] , τ
\end{code}
%</uncurryU>

%<*ports>
\AgdaTarget{ports}
\begin{code}
ports : ∀ {B} (τ : U B) {p₁ : ord 1 τ} → List (U B) × (U B)
ports τ = uncurryU τ
\end{code}
%</ports>





* For each element in U we get an Agda type. The types of circuit I/O form a SUBSET of Agda's types
<*El>
\AgdaTarget{El}
\begin{code}
El : ∀ {B} (τ : U B) → Set
El      𝟙          = ⊤
El {B}  ι          = B
El      (σ ⇒ τ)    = El σ  → El τ
El      (τ ⊗ υ)    = El τ  × El υ
El      (τ ⊕ υ)    = El τ  ⊎ El υ
El      (vec τ n)  = Vec (El τ) n
\end{code}
</El>


%<*defaults>
\AgdaTarget{defaults}
\begin{code}
defaults : ∀ {B} (b : B) (τ : U B) → El τ
defaults _ 𝟙          = tt
defaults b ι          = b
defaults b (σ ⇒ τ)    = const (defaults b τ)
defaults b (τ ⊗ υ)    = defaults b τ , defaults b υ
defaults b (τ ⊕ _)    = inj₁ (defaults b τ)
defaults b (vec τ n)  = replicate (defaults b τ)
\end{code}
%</defaults>


%<*Option>
\AgdaTarget{\_？}
\begin{code}
_？ : ∀ {B} (τ : U B) → U B
_？ τ = 𝟙 ⊕ τ
\end{code}
%</Option>



* A Ctxt is a sequence of codes (ex:  (𝟙 ◅ (σ ⊗ ρ) ◅ ε) )
\begin{code}
open module EnvUExp  B    = Environment (U B) using (Ctxt) public
open module EnvUImp  {B}  = Environment (U B) using (_∋_; Env; lookup) renaming (vz to Z; vs to S) public
\end{code}


%<*replicate-Ctxt>
\AgdaTarget{replicateCtxt}
\begin{code}
replicateCtxt : ∀ {B} (Γ : Ctxt B) (n : ℕ) (τ : U B) → Ctxt B
replicateCtxt Γ zero      _  = Γ
replicateCtxt Γ (suc n′)  τ  = τ ◅ replicateCtxt Γ n′ τ
\end{code}
%</replicate-Ctxt>


%<*Ctxt-length>
\AgdaTarget{lengthCtxt}
\begin{code}
lengthCtxt : ∀ {B} (Γ : Ctxt B) → ℕ
lengthCtxt ε         = zero
lengthCtxt (_ ◅ Γ′)  = suc (lengthCtxt Γ′)
\end{code}
%</Ctxt-length>


%<*ix>
\AgdaTarget{ix₀,ix₁,ix₂,ix₃,ix₄}
\begin{code}
ix₀ : ∀ {B} {Γ : Ctxt B} {α}             → α ◅ Γ ∋ α
ix₁ : ∀ {B} {Γ : Ctxt B} {α β}           → α ◅ β ◅ Γ ∋ β
ix₂ : ∀ {B} {Γ : Ctxt B} {α β δ}         → α ◅ β ◅ δ ◅ Γ ∋ δ
ix₃ : ∀ {B} {Γ : Ctxt B} {α β δ θ}       → α ◅ β ◅ δ ◅ θ ◅ Γ ∋ θ
ix₄ : ∀ {B} {Γ : Ctxt B} {α β δ θ κ}     → α ◅ β ◅ δ ◅ θ ◅ κ ◅ Γ ∋ κ
ix₀ = Z;  ix₁ = S ix₀;  ix₂ = S ix₁;  ix₃ = S ix₂;  ix₄ = S ix₃
\end{code}
%</ix>


%<*Ctxt-subeq>
\AgdaTarget{\_⊇\_}
\begin{code}
data _⊇_ {B} : (Δ Γ : Ctxt B) → Set where
  refl⊇  : ∀ {Γ}          → Γ ⊇ Γ
  ◅⊇     : ∀ {Δ Γ τ}      → Δ ⊇ Γ → (τ ◅ Δ) ⊇ Γ
  ◅⊇◅    : ∀ {Δ Γ τ}      → Δ ⊇ Γ → (τ ◅ Δ) ⊇ (τ ◅ Γ)
  flip⊇  : ∀ {Δ Γ τ₁ τ₂}  → Δ ⊇ Γ → (τ₁ ◅ τ₂ ◅ Δ) ⊇ (τ₂ ◅ τ₁ ◅ Γ)
\end{code}
%</Ctxt-subeq>


%<*Kix-subeq>
\AgdaTarget{Kix⊇}
\begin{code}
Kix⊇ : ∀ {B} {Δ Γ : Ctxt B} {τ} (p : Δ ⊇ Γ) → (Γ ∋ τ → Δ ∋ τ)
Kix⊇ refl⊇      i                               = i
Kix⊇ (◅⊇ p)     i                               = S (Kix⊇ p i)
Kix⊇ (◅⊇◅ _)    (done refl  ◅ _)                = Z
Kix⊇ (◅⊇◅ p)    (step tt    ◅ i)                = S (Kix⊇ p i)
Kix⊇ (flip⊇ p)  (done refl  ◅ _)                = S Z
Kix⊇ (flip⊇ p)  (step tt    ◅ done refl  ◅ _)   = Z
Kix⊇ (flip⊇ p)  (step tt    ◅ step tt    ◅ i)   = S (S (Kix⊇ p i))
\end{code}
%</Kix-subeq>


%<*Env⊇>
\AgdaTarget{Env⊇}
\begin{code}
Env⊇ : ∀ {B} {F : U B → Set} {Γ Δ : Ctxt B} (δ : Env F Δ) (γ : Env F Γ) (p : Δ ⊇ Γ) → Env F Δ
Env⊇ δ                 γ                 refl⊇       = γ
Env⊇ (↦ y ◅ δ′)        γ                 (◅⊇ p′)     = ↦ y ◅ Env⊇ δ′ γ p′
Env⊇ (↦ y ◅ δ′)        (↦ x ◅ γ′)        (◅⊇◅ p′)    = ↦ x ◅ Env⊇ δ′ γ′ p′   -- I think that x ≡ y here but how to get that fact...
Env⊇ (↦ y ◅ ↦ x ◅ δ′)  (↦ _ ◅ ↦ _ ◅ γ′)  (flip⊇ p′)  = ↦ y ◅ ↦ x ◅ Env⊇ δ′ γ′ p′
\end{code}
%</Env⊇>


%<*Ctxt-suffix>
\AgdaTarget{\_⊒\_}
\begin{code}
data _⊒_ {B} : (Δ Γ : Ctxt B) → Set where
  refl⊒  : ∀ {Γ′ Γ} (p : Γ′ ≡ Γ)      → Γ′ ⊒ Γ
  ◅⊒     : ∀ {Γ Δ τ} → (Δ⊒Γ : Δ ⊒ Γ)  → (τ ◅ Δ) ⊒ Γ
\end{code}
%</Ctxt-suffix>


%<*reIx0-given-inclusion>
\AgdaTarget{reIx₀⊒}
\begin{code}
reIx₀⊒ : ∀ {B} (Δ Γ : Ctxt B) τ → Δ ⊒ (τ ◅ Γ) → Δ ∋ τ
reIx₀⊒ .(τ ◅ Γ)  Γ τ (refl⊒ refl)  = Z
reIx₀⊒ (σ ◅ Δ′)  Γ τ (◅⊒ Δ′⊒τ◅Γ)   = S (reIx₀⊒ Δ′ Γ τ Δ′⊒τ◅Γ)
\end{code}
%</reIx0-given-inclusion>


* Assuming that Γ is a suffix of Δ and d = (length Δ) - (length Γ), otherwise stuck in postulate
%<*get-inclusion>
\AgdaTarget{get⊒}
\begin{code}
get⊒ : ∀ {B} (d : ℕ) (Δ Γ : Ctxt B) → Δ ⊒ Γ
get⊒ zero      Γ′        Γ = refl⊒ trustMe  -- FROM ASSUMPTION: Γ′ ≡ Γ when d = 0 (no guarantee of def. eq.)
get⊒ (suc d′)  (σ ◅ Δ′)  Γ = ◅⊒ {τ = σ} (get⊒ d′ Δ′ Γ)
get⊒ (suc d′)  ε         Γ = ☇ where postulate ☇ : ε ⊒ Γ  -- IMPOSSIBLE CASE: d > 0, Δ ⊒ Γ → Δ ≠ ε
\end{code}
%</get-inclusion>


%<*reIx0-index>
\AgdaTarget{reIx₀∋}
\begin{code}
reIx₀∋ : ∀ {B} τ (Γ Δ : Ctxt B) → Δ ∋ τ
reIx₀∋ τ Γ Δ = reIx₀⊒ Δ Γ τ (get⊒ d Δ (τ ◅ Γ))
  where d = lengthCtxt Δ ∸ suc (lengthCtxt Γ)
\end{code}
%</reIx0-index>
