\begin{code}
module Data.MapAccum where

open import Function using (_∘′_; id)
open import Data.Product using (_×_; _,_; uncurry) renaming (map to map×)
open import Data.Vec using (Vec; _∷_; []; unzip; zip)
open import Data.Stream using (Stream; _∷_)
open import Coinduction using (♭; ♯_)
\end{code}



%<*mapAccumL>
\AgdaTarget{mapAccumL}
\begin{code}
mapAccumL : ∀ {n ℓ ℓ₁ ℓ₂} {𝕊 : Set ℓ} {α : Set ℓ₁} {β : Set ℓ₂} (f : 𝕊 → α → (𝕊 × β)) (s : 𝕊) (xs : Vec α n) → 𝕊 × Vec β n
mapAccumL _ s []        =  s , []
mapAccumL f s (x ∷ xs′) =  let  s′ , y    = f s x
                                s″ , ys′  = mapAccumL f s′ xs′
                           in  s″ , (y ∷ ys′)
\end{code}
%</mapAccumL>



%<*mapAccumL1-derived>
\AgdaTarget{mapAccumL1}
\begin{code}
mapAccumL1 = mapAccumL
\end{code}
%</mapAccumL1-derived>


%<*swap12>
\AgdaTarget{swap12}
\begin{code}
swap12 : ∀ {ℓ₁ ℓ₂ ℓ₃} {α : Set ℓ₁} {β : Set ℓ₂} {γ : Set ℓ₃} → (α × (β × γ)) → (β × (α × γ))
swap12 (a , (b , c)) = b , (a , c)
\end{code}
%</swap12>


%<*transformF>
\AgdaTarget{transformF}
\begin{code}
transformF :  ∀ {ℓ₁ ℓ₂ ℓ₃ ℓ₄ ℓ₅} {α : Set ℓ₁} {β : Set ℓ₂} {φ : Set ℓ₃} {δ : Set ℓ₄} {Γ : Set ℓ₅}
              (f : β → Γ → α → φ → (β × α × δ)) → (Γ → α → β → φ → (α × β × δ))
transformF f γ x s y = swap12 (f s γ x y)
\end{code}
%</transformF>


\begin{code}
mapAccumL2 :  ∀ {n ℓ ℓ₁ ℓ₂ ℓ₃ ℓ₄} {𝕊 : Set ℓ} {α : Set ℓ₁} {γ : Set ℓ₂} {β : Set ℓ₃} {δ : Set ℓ₄}
              (f : 𝕊 → α → γ → (𝕊 × β × δ)) (s : 𝕊) (xs : Vec α n) (ys : Vec γ n) → 𝕊 × Vec β n × Vec δ n
mapAccumL2 f s xs ys = map×  id  unzip  (mapAccumL (uncurry ∘′ f) s (zip xs ys))
\end{code}



%<*mapAccumStream>
\AgdaTarget{mapAccumStream}
\begin{code}
mapAccumStream : ∀ {𝕊 α β : Set} (f : 𝕊 → α → (𝕊 × β)) (s : 𝕊) (xs : Stream α) → (Stream 𝕊 × Stream β)
mapAccumStream f s (x ∷ xs′) =  let  s′   , y    = f s x
                                     ss″  , ys′  = mapAccumStream f s′ (♭ xs′)
                                in (s′ ∷ ♯ ss″) , (y ∷ ♯ ys′)
\end{code}
%</mapAccumStream>
