Tested type-checking with:

* Stable:
  + Agda 2.5.2
  + Agda standard library 0.13

* Unstable:
  + Agda 2.6.0-587fe51
  + Agda standard library 0.13-f211eba
